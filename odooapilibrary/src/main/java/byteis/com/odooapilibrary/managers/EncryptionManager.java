package byteis.com.odooapilibrary.managers;

import android.content.Context;

import java.nio.charset.Charset;
import java.util.Random;

import byteis.com.odooapilibrary.data.SharedPref;
import byteis.com.odooapilibrary.util.UtilityMethods;
import byteis.com.odooapilibrary.util.encryption.AESencrp;

/**
 * Created by Dina on 23/10/2016.
 */

public class EncryptionManager {
    public static void savePassword(String password, Context context) {
        if (password != null && !"".equals(password.trim())) {
            byte[] encryptKeyValue = UtilityMethods.createRandomKey(new Random());
            try {
                SharedPref.SaveString(context, SharedPref.ENCRYPT_KEY, new String(encryptKeyValue, "UTF-8"));
                SharedPref.SaveString(context, SharedPref.PASSWORD_KEY, AESencrp.encrypt(password, encryptKeyValue));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static String getPassword(Context context) {
        byte[] encryptKey = SharedPref.LoadString(context, SharedPref.ENCRYPT_KEY).getBytes(Charset.forName("UTF-8"));
        String password = "";
        try {
            password = AESencrp.decrypt(SharedPref.LoadString(context, SharedPref.PASSWORD_KEY), encryptKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return password;
    }
}
