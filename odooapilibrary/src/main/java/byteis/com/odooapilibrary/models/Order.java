package byteis.com.odooapilibrary.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Dina on 18/10/2016.
 */

public class Order {

    @SerializedName("price_total")
    private double totalPrice;

    @SerializedName("date_confirm")
    private String date;

    @SerializedName("product_id")
    private ArrayList product;

    @SerializedName("partner_id")
    private ArrayList partner;


    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }


    public String getProductName() {
        String returnStr = "";
        if (product == null)
            returnStr = "";
        try {
            returnStr = (String) product.get(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnStr;
    }

    public String getpartnerName() {
        String returnStr = "";

        if (partner == null)
            returnStr = "";

        try {
            returnStr = (String) partner.get(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnStr;
    }

    public double getPartnerId() {
        double partnerId = 0;

        if (partner != null) {
            try {
                partnerId = (double) partner.get(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return partnerId;
    }


}
