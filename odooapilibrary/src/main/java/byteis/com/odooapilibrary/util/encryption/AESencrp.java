package byteis.com.odooapilibrary.util.encryption;

import android.util.Log;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class AESencrp {
//ToDO more security needed
    private static final String ALGO = "AES";
    private static final byte[] keyValue =
            new byte[]{'F', 'h', 'e', 'B', 'e', 's', 't',
                    'S', 'e', 'c', 'r', 'e', 't', 'W', 'e', 'y'};

    public static String encrypt(String Data,byte [] keyParam) throws Exception {
        Log.d("encryption+++++key", new String(keyParam,"UTF-8"));
        Key key = generateKey(keyParam);
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = c.doFinal(Data.getBytes());
        String encryptedValue = new BASE64Encoder().encode(encVal);
        Log.d("encryption+++++", encryptedValue + "  " + Data);
        return encryptedValue;
    }

    public static String decrypt(String encryptedData,byte [] keyParam) throws Exception {
        Log.d("decryption+++++key",new String(keyParam,"UTF-8"));
        Key key = generateKey(keyParam);
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.DECRYPT_MODE, key);
        byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedData);
        byte[] decValue = c.doFinal(decordedValue);
        String decryptedValue = new String(decValue);
        Log.d("decryption+++++", decryptedValue + "  " + encryptedData);
        return decryptedValue;
    }

    private static Key generateKey(byte [] keyParam) throws Exception {
        Key key = new SecretKeySpec(keyParam, ALGO);
        return key;
    }

}
