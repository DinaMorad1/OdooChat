package byteis.com.odooapilibrary.custom.json;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Created by Kimo on 23/10/2016.
 */

public class JsonRpcRequest<T> extends JsonRequest {
    private static final String JSONRPC_PARAM_ID = "id";
    private static final String JSONRPC_PARAM_METHOD = "method";
    private static final String JSONRPC_PARAM_PARAMETERS = "params";
    private static final String JSONRPC_PARAM_RPC_VERSION = "jsonrpc";


    public JsonRpcRequest(final String url, final String method, final JSONObject jsonRequest,
                          final Response.Listener<T> listener,
                          final Response.ErrorListener errorListener, final int requestId) throws JSONException {
        super(Method.POST, url, new JSONObject() {
            {
                putOpt(JSONRPC_PARAM_ID, requestId);
                putOpt(JSONRPC_PARAM_METHOD, "object");
                putOpt(JSONRPC_PARAM_PARAMETERS, jsonRequest);
                putOpt(JSONRPC_PARAM_RPC_VERSION, "2.0");
            }
        }.toString(), listener, errorListener);
    }

    @Override
    protected Response parseNetworkResponse(NetworkResponse response) {
        String je = null;
        try {
            je = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));
            return Response.success(new JSONObject(je), HttpHeaderParser.parseCacheHeaders(response));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}