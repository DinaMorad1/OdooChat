package byteis.com.odooapilibrary.custom.json;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Dina on 17/10/2016.
 */

public class JsonRpcResponse {
    private static final String JSONRPC_PARAM_ID = "id";
    private static final String JSONRPC_PARAM_ERROR = "error";
    public static final String JSONRPC_PARAM_RESULT = "result";

    private String mId;
    private JSONObject mError;
    private Object mResult;

    public JsonRpcResponse(final String jsonString) throws JSONException {
        final JSONObject jsonObject = new JSONObject(jsonString);

        mId = jsonObject.getString(JSONRPC_PARAM_ID);

        mError = jsonObject.optJSONObject(JSONRPC_PARAM_ERROR);

        if (mError != null) {
            return;
        }

        if (jsonObject.optJSONObject(JSONRPC_PARAM_RESULT) != null)
            mResult = jsonObject.optJSONObject(JSONRPC_PARAM_RESULT);
        else if (jsonObject.optJSONArray(JSONRPC_PARAM_RESULT) != null)
            mResult = jsonObject.optJSONArray(JSONRPC_PARAM_RESULT);
        else if (jsonObject.optInt(JSONRPC_PARAM_RESULT) != 0)
            mResult = jsonObject.optInt(JSONRPC_PARAM_RESULT);
    }

    public String getId() {
        return mId;
    }

    public JSONObject getError() {
        return mError;
    }

    public Object getResult() {
        return mResult;
    }

}
