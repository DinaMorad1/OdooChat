package byteis.com.odooapilibrary.api;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import byteis.com.odooapilibrary.models.Order;

/**
 * Created by Dina on 23/10/2016.
 */

public interface ApiClient {
    /*Odoo Fields*/
//    String ODOO_BASE_URL = "http://erp.byteis.com";//"http://36ea8f07.ngrok.io";
    public final static String ODOO_DATABASE_NAME = "dina";
//    public final static String ODOO_DATABASE_NAME = "byte";
    String SELF_HOSTED_URL = "/web/session/get_session_info";
    String DATABASE_LIST_URL = "/web/database/get_list";
//    String SESSION_INFO_URL = ODOO_BASE_URL + SELF_HOSTED_URL;

    String ODOO_LOGIN_URL = "/web/session/authenticate";
    String ODOO_SEARCH_URL = "/web/dataset/search_read";
    String ODOO_JSON_RPC = "/jsonrpc";
    String BOARD_MODEL_NAME = "board.board";
    String CUSTOM_VIEW_MODEL_NAME = "ir.ui.view.custom";
    String ACT_WINDOW = "ir.actions.act_window";
    String GROUP_BY = "groupby";
    String DOMAIN = "domain";
    String DISPLAY_NAME = "displayName";
    String MODEL_NAME = "modelName";
    String MEASURES = "measures";

    void odooLogin(String username, String password, String databaseName, ClientCallback<JSONObject> callback);

    void getSaleOrders(ClientCallback<Order> callback, boolean limit, int offset);

    void getDashboards(ClientCallback callback);

    void getCustomView(ClientCallback callback, int dashboardId);

    void getActionWindow(HashMap<Integer, HashMap<String, String>> actionsHashMap, ClientCallback callback);

    void getModelData(ClientCallback callback, String modelName, String domain);

    void getSessionInfo(ClientCallback callback, String inputUrl);

    void getDatabases(ClientCallback callback, String url);

    void getModelFields(ClientCallback callback, String model);

    void checkModelState(ClientCallback callback);

}
