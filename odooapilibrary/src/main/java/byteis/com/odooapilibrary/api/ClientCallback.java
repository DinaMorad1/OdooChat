package byteis.com.odooapilibrary.api;

/**
 * Created by Dina on 23/10/2016.
 */

public interface ClientCallback<T> {

    void onServerResultSuccess(T t);

    void onServerResultFailure(String failureMessage);
}
