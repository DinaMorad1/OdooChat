package byteis.com.odooapilibrary.api;

import android.content.Context;
import android.util.Log;
import android.util.Xml;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import byteis.com.odooapilibrary.data.SharedPref;
import byteis.com.odooapilibrary.managers.EncryptionManager;
import byteis.com.odooapilibrary.models.Order;

/**
 * Created by Dina on 23/10/2016.
 */

public class ApiClientImp extends VolleyHelper implements ApiClient {

    private static ApiClientImp instance;
    private HashMap<Integer, HashMap<String, String>> actionsHashMap = new HashMap<>();//by now return unique models--ToDo: to be changed


    protected ApiClientImp(Context context) {
        super(context);
    }

    public static ApiClientImp getInstance(Context context) {
        if (instance == null)
            instance = new ApiClientImp(context);
        return instance;
    }

    private String getRemoteUrl() {
        return SharedPref.LoadString(context, SharedPref.REMOTE_URL);
    }

    @Override
    public void odooLogin(String username, String password, String odooDatabaseName, ClientCallback<JSONObject> callback) {
        String url = getRemoteUrl() + ODOO_JSON_RPC;
        JSONObject params = new JSONObject();
        JSONArray allItemsJsonArray = new JSONArray();

        try {
            params.put("service", "common");
            params.put("method", "login");

            allItemsJsonArray.put(odooDatabaseName);
            allItemsJsonArray.put(username);
            allItemsJsonArray.put(password);

            params.put("args", allItemsJsonArray);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        //"common.login"
        scheduleJsonRpcObjectRequest("authenticate", url, params, callback, JSONObject.class, NORMAL_TIME_OUT);
    }

    public void getSaleOrders(ClientCallback<Order> callback, boolean limit, int offset) {
        String url = getRemoteUrl() + "/get_reports";//ODOO_JSON_RPC;

        /*search criteria filter*/
        JSONArray methodParams = new JSONArray();
        JSONArray paramsArray = new JSONArray();
        methodParams.put(paramsArray);

        /*needed fields to be returned from the model*/
        JSONArray showItems = new JSONArray();
        getList(methodParams, showItems, url, limit, offset, "board.board", callback, JSONObject.class);
    }

    private void getList(JSONArray searchJsonArray, JSONArray modelParamsArray, String url, boolean limit, int offset, String modelName, ClientCallback callback, Class classType) {
        JSONObject params = new JSONObject();

        try {
            /*needed fields to be returned from the model*/
            JSONObject kw = new JSONObject();

            /*service and method*/
            params.put("service", "object");
            params.put("method", "execute_kw");

            /*database and user*/
            JSONArray allItemsJsonArray = new JSONArray();
            allItemsJsonArray.put(SharedPref.LoadString(context, SharedPref.DATABASE_NAME));//database name
            allItemsJsonArray.put(SharedPref.LoadInt(context, SharedPref.USER_ID));/*user id*/
            allItemsJsonArray.put(EncryptionManager.getPassword(context));/*user password*/

            /*model name*/
            allItemsJsonArray.put(modelName);//model

            /*method name*/
            allItemsJsonArray.put("list");//method
            allItemsJsonArray.put(searchJsonArray);//search criteria
            allItemsJsonArray.put(kw);
            params.put("args", allItemsJsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        scheduleJsonRpcObjectRequest("call", url,
                params, callback, classType, NORMAL_TIME_OUT);
    }


    protected void searchRead(JSONArray searchJsonArray, JSONArray modelParamsArray, String url, boolean limit, int offset, String modelName, ClientCallback callback, Class classType) {
        JSONObject params = new JSONObject();

        try {
            /*needed fields to be returned from the model*/
            JSONObject kw = new JSONObject();
            kw.put("fields", modelParamsArray);

            /*offset and limit if needed for pagination*/
            kw.put("limit", limit);
            kw.put("offset", offset);

            /*service and method*/
            params.put("service", "object");
            params.put("method", "execute_kw");

            /*database and user*/
            JSONArray allItemsJsonArray = new JSONArray();
            allItemsJsonArray.put(SharedPref.LoadString(context, SharedPref.DATABASE_NAME));//database name
            allItemsJsonArray.put(SharedPref.LoadInt(context, SharedPref.USER_ID));/*user id*/
            allItemsJsonArray.put(EncryptionManager.getPassword(context));/*user password*/

            /*model name*/
            allItemsJsonArray.put(modelName);//model

            /*method name*/
            allItemsJsonArray.put("search_read");//method
            allItemsJsonArray.put(searchJsonArray);//search criteria
            allItemsJsonArray.put(kw);
            params.put("args", allItemsJsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        scheduleJsonRpcObjectRequest("call", url,
                params, callback, classType, NORMAL_TIME_OUT);
    }

    protected void invokeMethod(String method, JSONArray searchJsonArray, JSONArray modelParamsArray, String url, boolean limit, int offset, String modelName, ClientCallback callback, Class classType) {
        JSONObject params = new JSONObject();

        try {
            /*needed fields to be returned from the model*/
            JSONObject kw = new JSONObject();
//            kw.put("fields", modelParamsArray);

            /*offset and limit if needed for pagination*/
//            kw.put("limit", limit);
//            kw.put("offset", offset);

            /*service and method*/
            params.put("service", "object");
            params.put("method", "execute_kw");

            /*database and user*/
            JSONArray allItemsJsonArray = new JSONArray();
            allItemsJsonArray.put(SharedPref.LoadString(context, SharedPref.DATABASE_NAME));//database name
            allItemsJsonArray.put(SharedPref.LoadInt(context, SharedPref.USER_ID));/*user id*/
            allItemsJsonArray.put(EncryptionManager.getPassword(context));/*user password*/

            /*model name*/
            allItemsJsonArray.put(modelName);//model

            /*method name*/
            allItemsJsonArray.put(method);//method
            allItemsJsonArray.put(searchJsonArray);//search criteria
            allItemsJsonArray.put(kw);
            params.put("args", allItemsJsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        scheduleJsonRpcObjectRequest("call", url,
                params, callback, classType, NORMAL_TIME_OUT);
    }

    protected void searchRead(JSONArray searchJsonArray, JSONArray modelParamsArray, String url, boolean limit, int offset, String order, String modelName, ClientCallback callback, Class classType) {
        JSONObject params = new JSONObject();

        try {
            /*needed fields to be returned from the model*/
            JSONObject kw = new JSONObject();
            kw.put("fields", modelParamsArray);

            /*offset and limit if needed for pagination*/
            kw.put("limit", limit);
            kw.put("offset", offset);
            kw.put("order", order);

            /*service and method*/
            params.put("service", "object");
            params.put("method", "execute_kw");

            /*database and user*/
            JSONArray allItemsJsonArray = new JSONArray();
            allItemsJsonArray.put(SharedPref.LoadString(context, SharedPref.DATABASE_NAME));//database name
            allItemsJsonArray.put(SharedPref.LoadInt(context, SharedPref.USER_ID));/*user id*/
            allItemsJsonArray.put(EncryptionManager.getPassword(context));/*user password*/

            /*model name*/
            allItemsJsonArray.put(modelName);//model

            /*method name*/
            allItemsJsonArray.put("search_read");//method
            allItemsJsonArray.put(searchJsonArray);//search criteria
            allItemsJsonArray.put(kw);
            params.put("args", allItemsJsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        scheduleJsonRpcObjectRequest("call", url,
                params, callback, classType, NORMAL_TIME_OUT);
    }

    protected void post(JSONObject kw, JSONArray methodParams, String method, String url, String modelName, ClientCallback callback, Class classType) {
        JSONObject params = new JSONObject();

        try {
            /*service and method*/
            params.put("service", "object");
            params.put("method", "execute_kw");



            /*database and user*/
            JSONArray allItemsJsonArray = new JSONArray();
            allItemsJsonArray.put(SharedPref.LoadString(context, SharedPref.DATABASE_NAME));//database name
            allItemsJsonArray.put(SharedPref.LoadInt(context, SharedPref.USER_ID));/*user id*/
            allItemsJsonArray.put(EncryptionManager.getPassword(context));/*user password*/

            /*model name*/
            allItemsJsonArray.put(modelName);//model

            /*method name*/
            allItemsJsonArray.put(method);//method
            allItemsJsonArray.put(methodParams);
            allItemsJsonArray.put(kw);
            params.put("args", allItemsJsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        scheduleJsonRpcObjectRequest(method, url,
                params, callback, classType, NORMAL_TIME_OUT);
    }

    protected void postModel(JSONObject kw, String method, String url, String modelName, ClientCallback callback, Class classType) {
        JSONObject params = new JSONObject();

        try {
            /*service and method*/
            params.put("service", "object");
            params.put("method", "execute_kw");


            //db, uid, password, 'res.partner', 'create', [{
//            'name': "New Partner",

            /*database and user*/
            JSONArray allItemsJsonArray = new JSONArray();
            allItemsJsonArray.put(SharedPref.LoadString(context, SharedPref.DATABASE_NAME));//database name
            allItemsJsonArray.put(SharedPref.LoadInt(context, SharedPref.USER_ID));/*user id*/
            allItemsJsonArray.put(EncryptionManager.getPassword(context));/*user password*/

            /*model name*/
            allItemsJsonArray.put(modelName);//model

            /*method name*/
            allItemsJsonArray.put(method);//method
//            allItemsJsonArray.put(methodParams);

            JSONArray kwJsonArray = new JSONArray();
            kwJsonArray.put(kw);
            allItemsJsonArray.put(kwJsonArray);
            params.put("args", allItemsJsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        scheduleJsonRpcObjectRequest(method, url,
                params, callback, classType, NORMAL_TIME_OUT);
    }



    private void fieldsGet(JSONArray searchJsonArray, JSONArray modelParamsArray, String url, boolean limit, int offset, String modelName, ClientCallback callback, Class classType) {
        JSONObject params = new JSONObject();

        try {
            /*needed fields to be returned from the model*/
            JSONObject kw = new JSONObject();
            kw.put("attributes", modelParamsArray);

            /*service and method*/
            params.put("service", "object");
            params.put("method", "execute_kw");

            /*database and user*/
            JSONArray allItemsJsonArray = new JSONArray();
            allItemsJsonArray.put(SharedPref.LoadString(context, SharedPref.DATABASE_NAME));//database name
            allItemsJsonArray.put(SharedPref.LoadInt(context, SharedPref.USER_ID));/*user id*/
            allItemsJsonArray.put(EncryptionManager.getPassword(context));/*user password*/

            /*model name*/
            allItemsJsonArray.put(modelName);//model

            /*method name*/
            allItemsJsonArray.put("fields_get");//method
            allItemsJsonArray.put(searchJsonArray);//search criteria
            allItemsJsonArray.put(kw);
            params.put("args", allItemsJsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        scheduleJsonRpcObjectRequest("call", url,
                params, callback, classType, NORMAL_TIME_OUT);
    }

    @Override
    public void getDashboards(ClientCallback callback) {
        String url = getRemoteUrl() + ODOO_JSON_RPC;

         /*search criteria filter*/
        JSONArray methodParams = new JSONArray();
        JSONArray paramsArray = new JSONArray();

        /*filter to render parameters with confirmed state*/
        JSONArray itemsJsonArray = new JSONArray();
        itemsJsonArray.put("model");
        itemsJsonArray.put("=");
        itemsJsonArray.put("board.board");
        paramsArray.put(itemsJsonArray);
        methodParams.put(paramsArray);

        /*needed fields to be returned from the model*/
        JSONArray showItems = new JSONArray();
        boolean limit = false;
        int offset = 0;

        searchRead(methodParams, showItems, url, limit, offset, "ir.ui.view", callback, JSONObject.class);
    }

    @Override
    public void getModelFields(ClientCallback callback, String model) {
        String url = getRemoteUrl() + ODOO_JSON_RPC;

        JSONArray methodParams = new JSONArray();
        JSONArray paramsArray = new JSONArray();

         /*filter to render parameters with confirmed state*/
        methodParams.put(paramsArray);

        JSONArray showItems = new JSONArray();
        boolean limit = false;
        int offset = 0;
        fieldsGet(methodParams, showItems, url, limit, offset, model//"sale.order"
                , callback, JSONObject.class);
    }

    @Override
    public void getCustomView(final ClientCallback callback, int dashboardId) {
        String url = getRemoteUrl() + ODOO_JSON_RPC;

        JSONArray methodParams = new JSONArray();
        JSONArray paramsArray = new JSONArray();

         /*filter to render parameters with confirmed state*/
        JSONArray itemsJsonArray = new JSONArray();
        itemsJsonArray.put("ref_id");
        itemsJsonArray.put("=");
        itemsJsonArray.put(dashboardId);//as if it is the board id--find the relations that connects them
        paramsArray.put(itemsJsonArray);
        methodParams.put(paramsArray);

        JSONArray showItems = new JSONArray();
        boolean limit = false;
        int offset = 0;
        actionsHashMap = new HashMap<>();
        searchRead(methodParams, showItems, url, limit, offset, CUSTOM_VIEW_MODEL_NAME, new ClientCallback() {
            @Override
            public void onServerResultSuccess(Object object) {
                try {
                    JSONObject firstJsonObj = (JSONObject) ((JSONArray) object).get(0);//ToDo: handle the case when no items exist to avoid crash
                    String archStr = firstJsonObj.optString("arch");

                    //parsing the xml string
                    InputStream is = new ByteArrayInputStream(archStr.getBytes("UTF-8"));
                    XmlPullParser parser = Xml.newPullParser();
                    parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                    parser.setInput(is, null);
                    parser.nextTag();

                    int eventType = parser.getEventType();

                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        HashMap<String, String> valuesHashMap = new HashMap<>();
                        String name = null;
                        switch (eventType) {
                            case XmlPullParser.START_DOCUMENT:
                                break;
                            case XmlPullParser.START_TAG:
                                name = parser.getName();
                                if (name.equalsIgnoreCase("action")) {
                                    String contextText = parser.getAttributeValue("", "context");
                                    String actionIdStr = parser.getAttributeValue("", "name");
                                    JSONObject contextJsonObject = new JSONObject(contextText);
                                    String groupByStr = "";
                                    if (contextJsonObject.has("u'group_by'") && contextJsonObject.optJSONArray("u'group_by'") != null)
                                        groupByStr = contextJsonObject.optJSONArray("u'group_by'").getString(0);
                                    else if (contextJsonObject.has("group_by") && contextJsonObject.optJSONArray("group_by") != null)
                                        groupByStr = contextJsonObject.optJSONArray("group_by").getString(0);

                                    String displayName = parser.getAttributeValue("", "string");

                                    int actionId = Integer.parseInt(actionIdStr);
                                    valuesHashMap.put(GROUP_BY, groupByStr);
                                    valuesHashMap.put(DISPLAY_NAME, displayName);

                                    String domain = parser.getAttributeValue("", "domain");
                                    if (domain != null && !"".equals(domain))
                                        valuesHashMap.put(DOMAIN, domain);

                                    actionsHashMap.put(actionId, valuesHashMap);
                                }
                                break;
                            case XmlPullParser.END_TAG:
                                break;
                        }
                        eventType = parser.next();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                }

                getActionWindow(actionsHashMap, callback);
            }

            @Override
            public void onServerResultFailure(String failureMessage) {

            }
        }, JSONObject.class);


    }


    @Override
    public void getActionWindow(final HashMap<Integer, HashMap<String, String>> actionsHashMap, final ClientCallback callback) {
        if (actionsHashMap.size() == 0) {
            callback.onServerResultSuccess(actionsHashMap);
            return;
        }
        String url = getRemoteUrl() + ODOO_JSON_RPC;

        JSONArray methodParams = new JSONArray();
        JSONArray paramsArray = new JSONArray();

         /*filter to render parameters with confirmed state*/
        JSONArray itemsJsonArray;
        Iterator it = actionsHashMap.entrySet().iterator();
        if (actionsHashMap.size() > 1) {
            for (int i = 1; i < actionsHashMap.size(); i++) {
                paramsArray.put("|");
            }
        }

        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            itemsJsonArray = new JSONArray();
            itemsJsonArray.put("id");
            itemsJsonArray.put("=");
            itemsJsonArray.put(pair.getKey());
            paramsArray.put(itemsJsonArray);
        }

        methodParams.put(paramsArray);

        /*needed fields to be returned from the model*/
        JSONArray showItems = new JSONArray();
        boolean limit = false;
        int offset = 0;
        searchRead(methodParams, showItems, url, limit, offset, ACT_WINDOW, new ClientCallback() {
            @Override
            public void onServerResultSuccess(Object object) {

                JSONArray jsonArray = (JSONArray) object;
                JSONObject jsonObject = null;
                try {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = (JSONObject) jsonArray.get(i);
                        String modelName = jsonObject.optString("res_model");
                        int actionWindowId = jsonObject.optInt("id");
                        HashMap<String, String> paramsHashMap = actionsHashMap.get(actionWindowId);
                        if (paramsHashMap == null)
                            paramsHashMap = new HashMap<>();
                        paramsHashMap.put(MODEL_NAME, modelName);
                    }
                    callback.onServerResultSuccess(actionsHashMap)/*returning reports names knowing their model to requesting activity*/;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onServerResultFailure(String failureMessage) {
                Log.i("", failureMessage);
            }
        }, JSONObject.class);

    }

    @Override
    public void getModelData(ClientCallback callback, String modelName, String domain) {
        String url = getRemoteUrl() + ODOO_JSON_RPC;
        JSONArray methodParams = new JSONArray();
        JSONArray domainJsonArray = null;

        if (domain != null && !"".equals(domain.trim())) {
            try {
                domainJsonArray = new JSONArray(domain);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (domainJsonArray != null)
            methodParams.put(domainJsonArray);
        else {
            methodParams = new JSONArray();
            JSONArray paramsArray = new JSONArray();
            methodParams.put(paramsArray);
        }

        JSONArray showItems = new JSONArray();
        boolean limit = false;
        int offset = 0;
        searchRead(methodParams, showItems, url, limit, offset, modelName, callback, JSONObject.class);
    }

    @Override
    public void checkModelState(ClientCallback callback) {
        String url = getRemoteUrl() + ODOO_JSON_RPC;
        JSONArray methodParams = new JSONArray();
        JSONArray paramsArray = new JSONArray();
        JSONArray itemsJsonArray = new JSONArray();
        itemsJsonArray.put("model");
        itemsJsonArray.put("=");
        itemsJsonArray.put("sale.order");
        paramsArray.put(itemsJsonArray);
        methodParams.put(paramsArray);
        JSONArray showItems = new JSONArray();
        boolean limit = false;
        int offset = 0;
        searchRead(methodParams, showItems, url, limit, offset, "ir.model", callback, JSONObject.class);
    }

    @Override
    public void getSessionInfo(ClientCallback callback, String inputUrl) {
        String url = getRemoteUrl(inputUrl) + SELF_HOSTED_URL;
        JSONObject params = new JSONObject();
        scheduleJsonRpcObjectRequest("call", url, params, callback, JSONObject.class, NORMAL_TIME_OUT);
    }

    public String getRemoteUrl(String url) {
        if (!url.contains("http") && !url.contains("https"))
            url = "http://" + url;
        return url;
    }

    @Override
    public void getDatabases(ClientCallback callback, String inputUrl) {
        String url = getRemoteUrl(inputUrl) + ODOO_JSON_RPC;
        JSONObject params = new JSONObject();
        JSONArray allItemsJsonArray = new JSONArray();

        try {
            params.put("service", "db");
            params.put("method", "list");
            params.put("args", allItemsJsonArray);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        scheduleJsonRpcObjectRequest("call", url, params, callback, JSONObject.class, NORMAL_TIME_OUT);
    }


}
