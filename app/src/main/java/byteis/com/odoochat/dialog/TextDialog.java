package byteis.com.odoochat.dialog;


import android.app.Dialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.View;
import android.view.WindowManager;

import byteis.com.odoochat.R;
import byteis.com.odoochat.databinding.DialogTextBinding;

public class TextDialog extends Dialog {

    private DialogTextBinding binding;

    public TextDialog(Context context, String contentText) {
        super(context, R.style.dialog_theme);
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.dialog_text, null, false);
        setContentView(binding.getRoot());
        binding.contentTextView.setText(contentText);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        binding.btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
            }
        });


    }
}
