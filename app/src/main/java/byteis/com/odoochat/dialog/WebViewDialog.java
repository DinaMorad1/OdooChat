package byteis.com.odoochat.dialog;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import byteis.com.odoochat.R;
import byteis.com.odoochat.databinding.DialogWebViewBinding;


public class WebViewDialog extends Dialog {

    private DialogWebViewBinding binding;

    public WebViewDialog(Context context, String url) {
        super(context, R.style.dialog_theme);
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.dialog_web_view, null, false);
        setContentView(binding.getRoot());
        getWindow().setWindowAnimations(R.style.dialog_animation_fade);
//        getWindow().setGravity(Gravity.BOTTOM);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

        initWebView();
        binding.webView.loadUrl(url);
    }

    private void initWebView() {
        binding.webView.getSettings().setJavaScriptEnabled(true);
        binding.webView.getSettings().setBuiltInZoomControls(true);
        binding.webView.getSettings().setDisplayZoomControls(true);
        binding.webView.getSettings().setSupportZoom(true);
        binding.webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        binding.webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;

            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);

            }

            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                if (getActivity() != null && !getActivity().isFinishing()) {
//                    UtilityMethods.showSnackbar(description, binding.getRoot(), getActivity());
//                    binding.loadingProgressBar.setVisibility(View.GONE);
//                }
            }

            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }
        });

//        binding.webView.setWebChromeClient(new WebChromeClient() {

    }


}
