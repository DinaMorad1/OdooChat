package byteis.com.odoochat.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;


public class AbstractEmptyViewHolder extends RecyclerView.ViewHolder {
    public AbstractEmptyViewHolder(View itemView) {
        super(itemView);
    }
}