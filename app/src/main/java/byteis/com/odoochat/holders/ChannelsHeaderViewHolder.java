package byteis.com.odoochat.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import byteis.com.odoochat.R;

public class ChannelsHeaderViewHolder extends RecyclerView.ViewHolder {
    public TextView headerTitleTextView;

    public ChannelsHeaderViewHolder(View itemView) {
        super(itemView);
        headerTitleTextView = (TextView) itemView.findViewById(R.id.title);
    }
}