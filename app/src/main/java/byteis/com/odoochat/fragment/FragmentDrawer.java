package byteis.com.odoochat.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;

import java.util.ArrayList;
import java.util.List;

import byteis.com.odoochat.R;
import byteis.com.odoochat.adapter.ChannelsHeaderAdapter;
import byteis.com.odoochat.custom.NavDrawerItem;
import byteis.com.odoochat.data.SharedPref;
import byteis.com.odoochat.listeners.ChatUserSelectedListener;
import byteis.com.odoochat.models.Channel;
import byteis.com.odoochat.util.UtilityMethods;

public class FragmentDrawer extends android.app.Fragment {

    private static String TAG = FragmentDrawer.class.getSimpleName();
    public final static int HOME_POSITION = 0;
    public final static int PEOPLE_POSITION = 1;
    public final static int GROUPS_POSITION = 2;
    public final static int CHANNELS_POSITION = 3;
    public final static int LOGOUT_POSITION = 4;

    private RecyclerView recyclerView;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private ChannelsHeaderAdapter adapter;
    private View containerView;
    private static String[] titles = null;
    private View contentView;
    private ChatUserSelectedListener listener;
    private View layout;


    private ArrayList<Channel> channelList;

    public void setChannelList(ArrayList<Channel> channelList) {
        this.channelList = channelList;
        getData();
    }

    public FragmentDrawer() {
    }

    public void setHedarText(String headerText){
        ((TextView)layout.findViewById(R.id.user_name_text_view)).setText(headerText);
        String imgStr = SharedPref.LoadString(getActivity(), SharedPref.USER_IMAGE);
        byte[] decodedString = Base64.decode(imgStr, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        Bitmap imgBitmap = UtilityMethods.scaleBitmap(decodedByte, getActivity().getResources().getDimension(R.dimen.user_img_width_height), getActivity().getResources().getDimension(R.dimen.user_img_width_height));
        Bitmap roundedBitmap = UtilityMethods.getRoundedBitmap(imgBitmap, (int)getActivity().getResources().getDimension(R.dimen.user_img_width_height), (int)getActivity().getResources().getDimension(R.dimen.user_img_width_height));
        ((ImageView)layout.findViewById(R.id.image_view)).setImageBitmap(roundedBitmap);
    }

    public List<NavDrawerItem> getData() {
        List<NavDrawerItem> data = new ArrayList<>();

        // preparing navigation drawer items
        if (channelList != null) {
            for (Channel channel : channelList) {//titles.length
                NavDrawerItem navItem = new NavDrawerItem();
                navItem.setTitle(channel.getDisplayName());
//            navItem.setImageDrawable(imgsDrawable[i]);
                data.add(navItem);
            }
        }
        return data;
    }

    public ChannelsHeaderAdapter getAdapter() {
        return this.adapter;
    }

    public DrawerLayout getmDrawerLayout() {
        return this.mDrawerLayout;
    }

    public void setListener(ChatUserSelectedListener listener) {
        this.listener = listener;
        if (adapter != null)
            adapter.setListener(listener);
    }

//    public void setLoginUserData(Context context) {
//        try {
//            ParseUser loginUser = ParseUser.getCurrentUser();
//            if(loginUser != null) {
//                String avatarUrl = loginUser.getString(IUserService.AVATAR_URL);
//                if (avatarUrl != null) {
//                    new setImageAsyncTask(avatarUrl).execute();
//                }
//
//                TextView userNameTextView = (TextView) contentView.findViewById(R.id.fragment_navigation_drawer_txt_user_name);
//                userNameTextView.setText(loginUser.getUsername());
//
//                TextView userEmailTextView = (TextView)contentView.findViewById(R.id.fragment_navigation_drawer_txt_user_mail);
//                userEmailTextView.setText(loginUser.getEmail());
//            }
//        } catch (Exception e) {
//            ChatLogger.logHandledException(e);
//        }
//    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflating view layout
        layout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        recyclerView = (RecyclerView) layout.findViewById(R.id.drawerList);

        contentView = layout;
        adapter = new ChannelsHeaderAdapter(getActivity(), getData(), channelList, HOME_POSITION, listener);
        recyclerView.setAdapter(adapter);


        final StickyRecyclerHeadersDecoration headersDecor = new StickyRecyclerHeadersDecoration(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(headersDecor);

        return layout;
    }

    public void notifyAdapterDataChanged() {
        adapter.notifyDataChanged(channelList, getData());
    }

    public ChannelsHeaderAdapter getNavigationDrawerAdapter() {
        return this.adapter;
    }


    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                toolbar.setAlpha(1 - slideOffset / 2);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

    }
}