package byteis.com.odoochat.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import byteis.com.odoochat.R;
import byteis.com.odoochat.custom.NavDrawerItem;
import byteis.com.odoochat.holders.AbstractEmptyViewHolder;
import byteis.com.odoochat.holders.ChannelsHeaderViewHolder;
import byteis.com.odoochat.listeners.ChatUserSelectedListener;
import byteis.com.odoochat.models.Channel;

/**
 * Created by Dina on 15/11/2016.
 */

public class ChannelsHeaderAdapter extends RecyclerView.Adapter<ChannelsHeaderAdapter.ViewHolder> implements StickyRecyclerHeadersAdapter<RecyclerView.ViewHolder> {

    private ArrayList<Channel> channelsList = new ArrayList<>();
    private LayoutInflater inflater;
    private int itemPosition;
    private ChatUserSelectedListener listener;
    private List<NavDrawerItem> data = Collections.emptyList();

    public final int TYPE_ITEM = 0;
    public final int TYPE_HEADER = 1;
    public final int EMPTY_VIEW = 2;
    public final static String DIRECT_MESSAGES = "Direct Messages";
    public final static String PRIVATE_CHANNELS = "Private Channels";
    public final static String PUBLIC_CHANNELS = "Channels";
    private Context context;
    private int selectedItemPosition;


    public ChannelsHeaderAdapter(Context context, List<NavDrawerItem> data, ArrayList<Channel> channelsList, int selectedItemPosition, ChatUserSelectedListener listener) {
        this.channelsList = channelsList;
        inflater = LayoutInflater.from(context);
        this.itemPosition = selectedItemPosition;
        this.listener = listener;
        this.data = data;
        this.context = context;

    }

    public void setListener(ChatUserSelectedListener listener) {
        this.listener = listener;
    }

    public void notifyDataChanged(ArrayList<Channel> channelList, List<NavDrawerItem> navDrawerItems) {
        this.channelsList = channelList;
        this.data = navDrawerItems;
        notifyDataSetChanged();
    }

    @Override
    public ChannelsHeaderAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.nav_drawer_row, parent, false);
        ChannelsHeaderAdapter.ViewHolder holder = new ChannelsHeaderAdapter.ViewHolder(view);
//        view.setTag(String.valueOf(itemPosition));
        return holder;
    }

    @Override
    public void onBindViewHolder(ChannelsHeaderAdapter.ViewHolder holder, final int position) {
        NavDrawerItem current = data.get(position);
        holder.title.setText(current.getTitle());
        holder.holderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onChannelSelected(channelsList.get(position), position);
                }
//                sideMenuListener.onSideMenuItemSelected(position);
            }
        });

        if (position == this.selectedItemPosition) {
            holder.holderLayout.setBackgroundColor(ActivityCompat.getColor(context, R.color.light_green_color));
        } else {
            holder.holderLayout.setBackgroundColor(ActivityCompat.getColor(context, R.color.lightgrey));
        }
    }

    @Override
    public long getHeaderId(int position) {
        if (channelsList != null && channelsList.size() > 0) {
            Channel channel = channelsList.get(position);
            if (channel != null) {
                String headerText;

                if (channel.getChType().equalsIgnoreCase(Channel.DIRECT_MESSAGE_TYPE))
                    headerText = DIRECT_MESSAGES;
                else if (channel.getChType().equalsIgnoreCase(Channel.PRIVATE_CHANNEL_TYPE))
                    headerText = PRIVATE_CHANNELS;
                else
                    headerText = PUBLIC_CHANNELS;
                return headerText.charAt(1);
            }
        }
        return EMPTY_VIEW;
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        if (channelsList != null && channelsList.size() > 0) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.header_item_channels, parent, false);
            return new ChannelsHeaderViewHolder(view);
        } else {
            LinearLayout linearLayout = new LinearLayout(context);
            RecyclerView.ViewHolder viewHolder = new AbstractEmptyViewHolder(linearLayout);
            return viewHolder;//no header is needed in case of empty recyclerview
        }
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (channelsList != null && channelsList.size() > 0) {
            if (channelsList.get(position) != null) {
                Channel channel = channelsList.get(position);
                String headerText;

                if (channel.getChType().equalsIgnoreCase(Channel.DIRECT_MESSAGE_TYPE))
                    headerText = DIRECT_MESSAGES;
                else if (channel.getChType().equalsIgnoreCase(Channel.PRIVATE_CHANNEL_TYPE))
                    headerText = PRIVATE_CHANNELS;
                else
                    headerText = PUBLIC_CHANNELS;

                ((ChannelsHeaderViewHolder) holder).headerTitleTextView.setText(headerText);
            }
        }
    }

    @Override
    public int getItemCount() {
        return channelsList == null ? 0 : channelsList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView avatar;
        RelativeLayout holderLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.nav_drawer_row_title);
            avatar = (ImageView) itemView.findViewById(R.id.nav_drawer_row_img);
            holderLayout = (RelativeLayout) itemView;
        }
    }

    public void setSelectedItemPosition(int selectedItemPosition) {
        this.selectedItemPosition = selectedItemPosition;
        notifyDataSetChanged();
    }
}
