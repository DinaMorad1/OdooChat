package byteis.com.odoochat.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import byteis.com.odoochat.R;
import byteis.com.odoochat.custom.NavDrawerItem;
import byteis.com.odoochat.listeners.ChatUserSelectedListener;
import byteis.com.odoochat.models.Channel;
import byteis.com.odoochat.models.ChatUser;


public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.ViewHolder> {
    List<NavDrawerItem> data = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;
    private List<Channel> channelList;
    private int itemPosition;
    private int selectedItemPosition;
    private ChatUserSelectedListener listener;

    public NavigationDrawerAdapter(Context context, List<NavDrawerItem> data, int selectedItemPosition, List<Channel> channelList, ChatUserSelectedListener listener) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
        this.channelList = channelList;
//        this.sideMenuListener = sideMenuListener;
        this.selectedItemPosition = selectedItemPosition;
        this.listener = listener;
    }

    public void setListener(ChatUserSelectedListener listener){
        this.listener = listener;
    }

    public void notifyDataChanged(List<Channel> channelList, List<NavDrawerItem> navDrawerItems) {
        this.channelList = channelList;
        this.data = navDrawerItems;
        notifyDataSetChanged();
    }

//    public void setSideMenuListener(SideMenuListener sideMenuListener) {
//        this.sideMenuListener = sideMenuListener;
//    }

    @Override
    public int getItemViewType(int position) {
        this.itemPosition = position;
        return super.getItemViewType(position);
    }

    public void setSelectedItemPosition(int selectedItemPosition) {
        this.selectedItemPosition = selectedItemPosition;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.nav_drawer_row, parent, false);
        ViewHolder holder = new ViewHolder(view);
//        view.setTag(String.valueOf(itemPosition));
//        view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (listener != null) {
//                    int position = Integer.parseInt(view.getTag().toString());
//                    listener.onChannelSelected(channelList.get(position));
//                }
////                sideMenuListener.onSideMenuItemSelected(position);
//            }
//        });
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        NavDrawerItem current = data.get(position);
        holder.title.setText(current.getTitle());
        holder.holderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onChannelSelected(channelList.get(position), position);
                }
//                sideMenuListener.onSideMenuItemSelected(position);
            }
        });
//        holder.avatar.setImageDrawable(imgsDrawable[position]);
//
//        if (itemPosition == selectedItemPosition) {
//            holder.holderLayout.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.side_menu_selected_bg));
//        } else {
//            holder.holderLayout.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.side_menu_bg));
//        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView avatar;
        RelativeLayout holderLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.nav_drawer_row_title);
            avatar = (ImageView) itemView.findViewById(R.id.nav_drawer_row_img);
            holderLayout = (RelativeLayout) itemView;
        }
    }
}
