package byteis.com.odoochat.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import byteis.com.odoochat.R;
import byteis.com.odoochat.databinding.MessageCellBinding;
import byteis.com.odoochat.dialog.TextDialog;
import byteis.com.odoochat.dialog.WebViewDialog;
import byteis.com.odoochat.models.Attachment;
import byteis.com.odoochat.models.Message;
import byteis.com.odoochat.util.UtilityMethods;

/**
 * Created by Dina on 14/11/2016.
 */

public class MessagesAdapter extends BaseAdapter {

    private Context context;

    public MessagesAdapter(List<Message> messageList, Context context) {
        super(messageList);
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_EMPTY) {
            return super.onCreateViewHolder(parent, viewType);
        }

        if (layoutInflater == null)
            layoutInflater = LayoutInflater.from(parent.getContext());

        final MessageCellBinding binding =
                DataBindingUtil.inflate(layoutInflater, R.layout.message_cell, parent, false);
        return new ViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        super.onBindViewHolder(viewHolder, position);
        int type = getItemViewType(position);

        if (type == TYPE_EMPTY)
            return;

        final ViewHolder holder = (ViewHolder) viewHolder;
        final Message message = (Message) items.get(position);
        holder.binding.messageTextView.setText(Html.fromHtml(message.getText()));

        if (message.getAvatar() != null) {
            byte[] decodedString = Base64.decode(message.getAvatar(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            Bitmap imgBitmap = UtilityMethods.scaleBitmap(decodedByte, context.getResources().getDimension(R.dimen.user_img_width_height), context.getResources().getDimension(R.dimen.user_img_width_height));
            holder.binding.userImage.setImageBitmap(imgBitmap);
        } else
            holder.binding.userImage.setImageBitmap(null);

        if (message.getFromName() != null && !"".equals(message.getFromName())) {
            holder.binding.messageSenderTextView.setText(message.getFromName());
            holder.binding.messageSenderTextView.setVisibility(View.VISIBLE);
        } else {
            holder.binding.messageSenderTextView.setVisibility(View.GONE);
            holder.binding.messageSenderTextView.setText("");
        }

        if (message.getWriteDate() != null && !"".equals(message.getWriteDate())) {
            String formattedDate = UtilityMethods.getFormattedDate(context, message.getWriteDateLong());
            holder.binding.messageDateTextView.setText(formattedDate);
        } else
            holder.binding.messageDateTextView.setText("");

        holder.binding.attachmentLayout.removeAllViews();

        if (message.getAttachmentArrayList() != null && message.getAttachmentArrayList().size() > 0) {
            holder.binding.imageScrollView.setVisibility(View.VISIBLE);
            ImageView imageView;
            TextView textView;

            for (int index = 0; index < message.getAttachmentArrayList().size(); index++) {
                final Attachment attachment = message.getAttachmentArrayList().get(index);

                if (attachment.getMimeType() != null && attachment.getMimeType().equalsIgnoreCase(Attachment.IMAGE_TYPE)) {
                    imageView = new ImageView(context);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) context.getResources().getDimension(R.dimen.attachment_height));
                    params.setMargins(0, 0, (int) context.getResources().getDimension(R.dimen.attachment_margin), 0);
                    imageView.setLayoutParams(params);
                    int bitmapwidth = UtilityMethods.getBitmapWidth(attachment.getBitmap(), (int) context.getResources().getDimension(R.dimen.attachment_height));
                    imageView.setImageBitmap(UtilityMethods.scaleBitmap(attachment.getBitmap(), bitmapwidth, (int) context.getResources().getDimension(R.dimen.attachment_height)));
                    holder.binding.attachmentLayout.addView(imageView);
                } else if (attachment.getMimeType() != null && attachment.getMimeType().equalsIgnoreCase(Attachment.TEXT_TYPE)) {
                    textView = new TextView(context);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, (int) context.getResources().getDimension(R.dimen.attachment_height));
                    params.setMargins(0, 0, (int) context.getResources().getDimension(R.dimen.attachment_margin), 0);
                    textView.setLayoutParams(params);
                    textView.setSingleLine(false);
                    textView.setMaxLines(2);
                    textView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.border_drawable));
                    textView.setText(attachment.getAttachmentText());

                    textView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            TextDialog textDialog = new TextDialog(context, attachment.getAttachmentText());
                            textDialog.show();
                        }
                    });

                    holder.binding.attachmentLayout.addView(textView);
                } else if (attachment.getName() != null && !"".equals(attachment.getName())) {
                    textView = new TextView(context);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(0, 0, (int) context.getResources().getDimension(R.dimen.attachment_margin), 0);
                    textView.setLayoutParams(params);
                    textView.setSingleLine(true);
//                    textView.setPadding((int) context.getResources().getDimension(R.dimen.attachment_padding), (int) context.getResources().getDimension(R.dimen.attachment_padding), (int) context.getResources().getDimension(R.dimen.attachment_padding), (int) context.getResources().getDimension(R.dimen.attachment_padding));
//                    textView.setMaxLines(2);
                    textView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.border_drawable));
                    textView.setText(attachment.getName());
                    holder.binding.attachmentLayout.addView(textView);
                    final int finalIndex = index;
                    textView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            byte[] bytes = Base64.decode(attachment.getDatas(), Base64.DEFAULT);
                            File fileFromBytes;
                            ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
                            ObjectInputStream ois = null;
                            try {
                                ois = new ObjectInputStream(bis);
                                fileFromBytes = (File) ois.readObject();
                                bis.close();
                                ois.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (ClassNotFoundException e) {
                                e.printStackTrace();
                            }
                            File file = new File(attachment.getName());

                            String mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(attachment.getMimeType());

                            Intent intent = new Intent();
                            intent.setAction(android.content.Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.fromFile(file), mime);
                            ((Activity)context).startActivityForResult(intent, 10);

                        }
                    });
                }
            }
        } else {
            if (message.getAttachments() == null || message.getAttachments().size() == 0)
                holder.binding.imageScrollView.setVisibility(View.GONE);
            else {
                holder.binding.imageScrollView.setVisibility(View.VISIBLE);
                View view = new View(context);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((int) context.getResources().getDimension(R.dimen.attachment_height), (int) context.getResources().getDimension(R.dimen.attachment_height));
                view.setLayoutParams(params);
                view.setBackgroundDrawable(ActivityCompat.getDrawable(context, R.drawable.border_drawable));
                holder.binding.attachmentLayout.addView(view);
            }
        }
    }


    public void updateItem(int position, Message message) {
        items.set(position, message);
        notifyItemChanged(position);
    }

    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final MessageCellBinding binding;

        ViewHolder(final MessageCellBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
