package byteis.com.odoochat.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import byteis.com.odoochat.R;


public class DatabasesSpinnerAdapter extends ArrayAdapter<String> {

    private String[] values;
    private Context context;

    public DatabasesSpinnerAdapter(Context context, int resource, String[] values) {
        super(context, resource);
        this.values = values;
        this.context = context;
    }

    public int getCount() {
        return values.length;
    }

    public String getItem(int position) {
        return values[position];
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout spinnerParentLayout = (LinearLayout) ((Activity) context).getLayoutInflater().inflate(R.layout.spinner_databases_label, null, false);
        TextView itemTextView = (TextView) spinnerParentLayout.findViewById(R.id.measure_text_view);
        itemTextView.setText(values[position]);
        return spinnerParentLayout;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        LinearLayout spinnerParentLayout = (LinearLayout) ((Activity) context).getLayoutInflater().inflate(R.layout.spinner_databases_label, null, false);
        TextView itemTextView = (TextView) spinnerParentLayout.findViewById(R.id.measure_text_view);
        itemTextView.setTextColor(context.getResources().getColor(R.color.secondary_txt_color));
        itemTextView.setText(values[position]);
        return spinnerParentLayout;
    }

    public void setItems(String[] measures) {
        this.values = measures;
        notifyDataSetChanged();
    }
}

