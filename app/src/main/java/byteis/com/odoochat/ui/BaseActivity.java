package byteis.com.odoochat.ui;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Slide;
import android.transition.Transition;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import byteis.com.odoochat.R;
import byteis.com.odoochat.api.ApiClient;
import byteis.com.odoochat.api.ApiClientImp;
import byteis.com.odoochat.data.SharedPref;
import byteis.com.odoochat.databinding.ActivityBaseBinding;
import byteis.com.odoochat.managers.UsersManager;
import byteis.com.odoochat.models.ServerResponse;
import byteis.com.odoochat.util.UtilityMethods;

/**
 * Created by Dina on 09/10/2016.
 */

public class BaseActivity extends AppCompatActivity {

    protected ActivityBaseBinding baseBinding;
    protected ApiClient apiClient;
    protected ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_base, null, false);
        setContentView(baseBinding.getRoot());
        apiClient = ApiClientImp.getInstance(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.primary_color_dark));
        }

        applyTransition(this);
        initToolbar();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    protected Activity applyTransition(Activity activity) {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            Transition transition = new Slide();
            ((Slide) transition).setSlideEdge(Gravity.RIGHT);
            transition.excludeTarget(android.R.id.statusBarBackground, true);
            transition.excludeTarget(android.R.id.navigationBarBackground, true);
            activity.getWindow().setEnterTransition(transition);
//            activity.getWindow().setExitTransition(transition);
        }
        return activity;
    }

    private void initToolbar() {
        if (baseBinding.toolbar != null)
            setSupportActionBar(baseBinding.toolbar.toolbar);
    }

    public void initToolbar(Toolbar toolbar){
        if (toolbar != null)
            setSupportActionBar(toolbar);
    }

    protected void inflateContent(View view) {
        RelativeLayout contentContainerLayout = baseBinding.baseCustomView;
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        contentContainerLayout.removeAllViews();
        contentContainerLayout.addView(view);
    }

    protected void setError(TextInputLayout textInputLayout, String error) {
        textInputLayout.setError(error);
        textInputLayout.getEditText().setError(error);
    }

    protected void showProgressDialog(String dialogMessage, boolean isCancelable, boolean isIndeteminate) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(this);

        if (!progressDialog.isShowing()) {
            progressDialog.setMessage(dialogMessage);
            progressDialog.setCancelable(isCancelable);
            progressDialog.setIndeterminate(isIndeteminate);
            progressDialog.show();
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void startActivityWithTransition(Activity activity, Intent intent) {
        ActivityOptionsCompat options = null;
        try {
            options = getOptionsCompat(activity);
            ActivityCompat.startActivity(activity, intent, options.toBundle());
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    private ActivityOptionsCompat getOptionsCompat(Activity activity) {
        ActivityOptionsCompat options = null;

        try {
            options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, null);
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        applyTransition(activity);
        return options;
    }

    protected void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    protected void onServerFailure(ServerResponse serverResponse, int statusCode) {
        String errorMessage = getString(R.string.something_wrong);
        if (serverResponse != null && serverResponse.getErrorMessageStr() != null && !"".equals((serverResponse.getErrorMessageStr()).trim()))
            errorMessage = serverResponse.getErrorMessageStr();

        if (!isFinishing()) {
            UtilityMethods.showSnackbar(errorMessage, getWindow().getDecorView(), this);
            dismissProgressDialog();
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    protected void finishActivity(Activity activity) {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            activity.finishAfterTransition();
        } else {
            activity.finish();
        }
    }

    protected void setToolBarContent(String title, boolean showTitle, boolean showHome) {
        if (!"".equals(title)) {
            getSupportActionBar().setTitle(title);
        }
        getSupportActionBar().setDisplayShowTitleEnabled(showTitle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(showHome);
    }

    protected void signout(){
        SharedPref.deleteShared(BaseActivity.this);
        UsersManager.getInstance().removeLoginUser();
        Intent i = new Intent(BaseActivity.this, LoginActivity.class);
        startActivityWithTransition(BaseActivity.this, i);
        finishActivity(BaseActivity.this);
    }

    protected void closeSoftKeypad() {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }


    protected boolean hasPermission(String permission) {
        int permissionCheckState = ContextCompat.checkSelfPermission(this,
                permission);

        if (permissionCheckState == PackageManager.PERMISSION_GRANTED)
            return true;

        return false;
    }

    protected void showServerFailureError(String serverError, View view){
        UtilityMethods.showSnackbar(serverError, view, this);
    }

    protected void requestPermission(final String permission, String permissionExplanation, final int requestCode) {

        boolean explainPermission = ActivityCompat.shouldShowRequestPermissionRationale(this,
                permission);

        if (explainPermission) {
            //Explain to the user why we need this permission
            Snackbar snackbar = UtilityMethods.getSnackbar(permissionExplanation, baseBinding.getRoot(), this);
            snackbar.setAction("Ok", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    requestPermission(permission, requestCode);
                }
            });
            snackbar.show();
            return;
        }
        requestPermission(permission, requestCode);
    }

    private void requestPermission(String permission, int requestCode) {
        ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
    }
}
