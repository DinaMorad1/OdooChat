package byteis.com.odoochat.ui;

import android.os.Bundle;
import android.util.Log;

import org.json.JSONArray;

import java.util.ArrayList;

import byteis.com.odooapilibrary.api.ClientCallback;
import byteis.com.odoochat.R;
import byteis.com.odoochat.models.Message;

public class ChatActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        apiClient.getUsers(new ClientCallback() {
            @Override
            public void onServerResultSuccess(Object object) {
                Log.i("", "");
            }

            @Override
            public void onServerResultFailure(String failureMessage) {
                Log.i("", "");
            }
        });

        apiClient.getUserSessions(7, new ClientCallback() {
            @Override
            public void onServerResultSuccess(Object object) {
                JSONArray messagesJsonArray = (JSONArray) object;
//                ArrayList<Message> messagesList = Message.fromJson(messagesJsonArray);
//                Log.i("", "messages size is : " + messagesList);
            }

            @Override
            public void onServerResultFailure(String failureMessage) {
                Log.i("", "failure");
            }
        });

    }
}
