package byteis.com.odoochat.ui;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;

import byteis.com.odooapilibrary.data.SharedPref;
import byteis.com.odoochat.R;
import byteis.com.odoochat.databinding.ActivitySplashBinding;

public class SplashActivity extends BaseActivity {

//    private ActivitySplashBinding binding;

    private ActivitySplashBinding binding;
    private final int SPLASH_DISPLAY_LENGTH = 1500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataBindingUtil.setContentView(this, R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                /*handle activity to another activity navigation view on UI thread.*/
                    SplashActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                navigateActivity();
                            } catch (RuntimeException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (RuntimeException e) {
                    e.printStackTrace();
                }
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    private void navigateActivity() {
        if (SharedPref.LoadInt(SplashActivity.this, SharedPref.USER_ID) != 0)
            startActivityWithTransition(this, new Intent(this, ChattingActivity.class));
        else
            startActivityWithTransition(this, new Intent(this, LoginActivity.class));
        finishActivity(this);
    }
}
