package byteis.com.odoochat.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import byteis.com.odooapilibrary.api.ClientCallback;
import byteis.com.odooapilibrary.api.VolleyHelper;
import byteis.com.odoochat.R;
import byteis.com.odoochat.adapter.MessagesAdapter;
import byteis.com.odoochat.custom.RealPathUtil;
import byteis.com.odoochat.data.SharedPref;
import byteis.com.odoochat.databinding.ActivityChattingBinding;
import byteis.com.odoochat.fragment.FragmentDrawer;
import byteis.com.odoochat.listeners.ChatUserSelectedListener;
import byteis.com.odoochat.models.Attachment;
import byteis.com.odoochat.models.Channel;
import byteis.com.odoochat.models.Message;
import byteis.com.odoochat.util.UtilityMethods;

public class ChattingActivity extends BaseActivity
        implements ChatUserSelectedListener {

    private ActivityChattingBinding binding;
    private MessagesAdapter messagesAdapter;
    private FragmentDrawer fragmentDrawer;
    private int selectedChannelId;
    private static final int FILE_SELECT_CODE = 0;
    private Channel selectedChannel;
    public final static int REQUEST_CAMERA_CODE = 22;
    public final static int REQUEST_STORAGE_CODE = 23;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_chatting);
        binding.appBarChatting.chatLayout.swipeContainer.setEnabled(false);
        setSupportActionBar(binding.appBarChatting.toolbar);
        binding.appBarChatting.chatLayout.swipeContainer.setRefreshing(true);

        initFragmentDrawer();

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, binding.drawerLayout, binding.appBarChatting.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                closeSoftKeypad();
            }
        };
        binding.drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        apiClient.getFilteredChannels(new ClientCallback() {
            @Override
            public void onServerResultSuccess(Object object) {
                JSONObject jsonObject = (JSONObject) object;
                JSONArray directMessagesJsonArray = jsonObject.optJSONArray(Channel.DIRECT_MESSAGE_TYPE);
                JSONArray privateChannelJsonArray = jsonObject.optJSONArray(Channel.PRIVATE_CHANNEL_TYPE);
                JSONArray publicChannelsJsonArray = jsonObject.optJSONArray(Channel.PUBLIC_CHANNEL_TYPE);

                ArrayList<Channel> directMessages = Channel.fromJson(directMessagesJsonArray, Channel.DIRECT_MESSAGE_TYPE);
                ArrayList<Channel> privateChannels = Channel.fromJson(privateChannelJsonArray, Channel.PRIVATE_CHANNEL_TYPE);
                ArrayList<Channel> publicChannels = Channel.fromJson(publicChannelsJsonArray, Channel.PUBLIC_CHANNEL_TYPE);
                ArrayList allChannel = new ArrayList();

                allChannel.addAll(publicChannels);
                allChannel.addAll(directMessages);
                allChannel.addAll(privateChannels);
                fragmentDrawer.setChannelList(allChannel);
                fragmentDrawer.setListener(ChattingActivity.this);
                fragmentDrawer.notifyAdapterDataChanged();
//                        if (channelList != null && channelList.size() > 0)
//                            onChannelSelected(channelList.get(0), 0);
//                        changeSideMenuSelection(0);//select first item

            }

            @Override
            public void onServerResultFailure(String failureMessage) {
                Log.i("", "");
            }
        });

//        apiClient.getChannels(new ClientCallback() {
//            @Override
//            public void onServerResultSuccess(Object object) {
//                final ArrayList<Channel> privateChannelsList = new ArrayList<>();
//                final ArrayList<Channel> publicChannelsList = new ArrayList<>();
//                final ArrayList<Channel> directMessages = new ArrayList<>();
//
//                final ArrayList<Channel> channelList = new ArrayList<>();
//
//                ArrayList<Integer> partnerIdsList = new ArrayList<Integer>();
//
//                JSONArray channelsJsonArray = (JSONArray) object;
//                JSONObject channelJsonObj;
//
//                for (int i = 0; i < channelsJsonArray.length(); i++) {
//                    try {
//                        channelJsonObj = channelsJsonArray.getJSONObject(i);
//                        Channel channel = gson.fromJson(channelJsonObj.toString(), Channel.class);
//                        String privacyState = channelJsonObj.getString("public");
//                        boolean isMember = channelJsonObj.optBoolean("is_member");
//                        String channelType = channelJsonObj.optString("channel_type");
//
//                        if (isMember) {
//                            if (channelType != null && channelType.equalsIgnoreCase("chat")) {
//                                channel.setDirectMessage(true);
//                                directMessages.add(channel);
//                                ArrayList partnerIds = channel.getChannelPartnerIds();
//                                for (Object item : partnerIds) {
//                                    double itemDouble = Double.parseDouble(item.toString());
//                                    int id = (int) itemDouble;
////                                    if (id != loginUserId)
//                                    partnerIdsList.add(id);
//                                }
//                            } else
//                                channel.setDirectMessage(false);
//
//                            if (privacyState.equalsIgnoreCase("private")) {
//                                channel.setPrivate(true);
//                                if (!channel.isDirectMessage())
//                                    privateChannelsList.add(channel);
//                            } else {
//                                channel.setPrivate(false);
//                                publicChannelsList.add(channel);
//                            }
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//
//                }
//
//
//                apiClient.getPartners(partnerIdsList, new ClientCallback() {
//                    @Override
//                    public void onServerResultSuccess(Object object) {
//                        JSONArray partnersJsonArray = (JSONArray) object;
//                        JSONObject partnerJsonObj;
//                        String loginUserName = SharedPref.LoadString(ChattingActivity.this, SharedPref.USER_NAME);
//                        for (int i = 0; i < partnersJsonArray.length(); i++) {
//                            try {
//                                partnerJsonObj = partnersJsonArray.getJSONObject(i);
//                                if (partnerJsonObj != null) {
//                                    int partnerId = partnerJsonObj.getInt("id");
//                                    String displayName = "";
//
//                                    for (int index = 0; index < directMessages.size(); index++) {
//                                        if (directMessages.get(index).getChannelPartnerIds().contains(partnerId)) {
//                                            Channel ch = directMessages.get(index);
//                                            displayName = partnerJsonObj.getString("name");
//
//                                            if (!displayName.trim().equalsIgnoreCase(loginUserName)) {
//                                                ch.setDisplayName(displayName);
//                                                directMessages.set(index, ch);
//                                            }
//                                        }
//                                    }
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }
//
//                        channelList.addAll(publicChannelsList);
//                        channelList.addAll(privateChannelsList);
//                        channelList.addAll(directMessages);
//
//                        fragmentDrawer.setChannelList(channelList);
//                        fragmentDrawer.setListener(ChattingActivity.this);
//                        fragmentDrawer.notifyAdapterDataChanged();
//                        if (channelList != null && channelList.size() > 0)
//                            onChannelSelected(channelList.get(0), 0);
////                        changeSideMenuSelection(0);//select first item
//                    }
//
//                    @Override
//                    public void onServerResultFailure(String failureMessage) {
//
//                    }
//                });

//
//            }
//
//            @Override
//            public void onServerResultFailure(String failureMessage) {
//                Log.i("", "");
//            }
//        }, "channel", "groups", true);


    }

    private void startCameraIntent() {
        UtilityMethods.createImage(this);

        final List<Intent> cameraIntents = new ArrayList<Intent>();
        final Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, UtilityMethods.outputFileUri);
            cameraIntents.add(intent);
        }

        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));
        int YOUR_SELECT_PICTURE_REQUEST_CODE = 1;
        startActivityForResult(chooserIntent, YOUR_SELECT_PICTURE_REQUEST_CODE);
    }

    private void initFragmentDrawer() {
        fragmentDrawer = (FragmentDrawer) getFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        String loginUserName = SharedPref.LoadString(ChattingActivity.this, SharedPref.USER_NAME);
        fragmentDrawer.setHedarText(loginUserName);
    }

    @Override
    public void onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.chatting, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            signout();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*
    * sidemenu
    * */
    private void closeSideMenu() {
        binding.drawerLayout.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onChannelSelected(Channel channel, int selectedSideMenuItemPosition) {
        this.selectedChannel = channel;
        if (messagesAdapter != null && messagesAdapter.getItems() != null)
            messagesAdapter.clearItems();
        binding.appBarChatting.chatLayout.swipeContainer.setRefreshing(true);
        binding.appBarChatting.chatLayout.messageEditText.setHint("Message @" + channel.getDisplayName());
        this.selectedChannelId = channel.getId();
        setToolBarContent(channel.getDisplayName(), true, false);
        closeSideMenu();
        fragmentDrawer.getAdapter().setSelectedItemPosition(selectedSideMenuItemPosition);

        apiClient.getChannelMessages(channel.getId(), new ClientCallback() {
            @Override
            public void onServerResultSuccess(Object object) {
                Log.i("", "");
                binding.appBarChatting.chatLayout.swipeContainer.setRefreshing(false);
                Log.i("", "success");
                JSONArray messagesJsonArray = (JSONArray) object;

                final ArrayList<Message> messageList = getMessagesList(messagesJsonArray);

                if (messagesAdapter == null) {
                    initRecyclerView();
                    messagesAdapter = new MessagesAdapter(messageList, ChattingActivity.this);
                    binding.appBarChatting.chatLayout.chatRecyclerView.setAdapter(messagesAdapter);
                } else
                    messagesAdapter.setItems(messageList);

                binding.appBarChatting.chatLayout.chatRecyclerView.scrollToPosition(messageList.size() - 1);


                /*retrieve message attachment*/
                //ToDo: to be changed with having position of messages having attachment
                for (int index = 0; index < messageList.size(); index++) {
                    final Message message = messageList.get(index);

                    if (message.getTrackingCodes() != null && message.getTrackingCodes().size() > 0) {
                        final int finalIndex = index;
                        apiClient.getTrackingCodes(message.getTrackingCodes(), new ClientCallback() {
                            @Override
                            public void onServerResultSuccess(Object object) {
                                if (object != null) {
                                    String messageText = "";
                                    JSONArray trackingCodeIds = (JSONArray) object;
                                    for (int i = 0; i < trackingCodeIds.length(); i++) {
                                        try {
                                            JSONObject trackingCodeObj = trackingCodeIds.getJSONObject(i);
                                            messageText += trackingCodeObj.getString("field_desc") + ": " + trackingCodeObj.getString("new_value_char") + " <br/>";
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                    Message responseMessage = messageList.get(finalIndex);
                                    responseMessage.setText(messageText);
                                    messagesAdapter.updateItem(finalIndex, responseMessage);
//                                    binding.appBarChatting.chatLayout.chatRecyclerView.scrollToPosition(messagesAdapter.getItems().size());
                                }
                            }

                            @Override
                            public void onServerResultFailure(String failureMessage) {

                            }
                        });
                    }


                    if (message.getAttachments() != null && message.getAttachments().size() > 0) {
                        final int finalIndex = index;
                        apiClient.getAttachment(message.getAttachments(), new ClientCallback() {
                            @Override
                            public void onServerResultSuccess(Object object) {
                                Message updateMessage = messageList.get(finalIndex);

                                JSONArray attachmentsJsonArray = (JSONArray) object;
                                for (int i = 0; i < attachmentsJsonArray.length(); i++) {
                                    try {
                                        JSONObject attachmentJsonObj = attachmentsJsonArray.getJSONObject(i);
                                        String attachmentStr = attachmentJsonObj.optString("datas");
                                        String mimeType = attachmentJsonObj.optString("mimetype");
                                        if (attachmentStr != null && !"".equalsIgnoreCase(attachmentStr)) {
                                            Attachment attachment = new Attachment();
                                            attachment.setMimeType(mimeType);
                                            attachment.setDatas(attachmentStr);
                                            if (attachmentJsonObj.optString("url") != null)
                                                attachment.setUrl(attachmentJsonObj.optString("url"));

                                            if (attachmentJsonObj.optString("name") != null)
                                                attachment.setName(attachmentJsonObj.optString("name"));


                                            if (mimeType != null) {
                                                if (mimeType.equalsIgnoreCase("image/jpeg")) {
                                                    //get bitmap
                                                    Bitmap imgBitmap = UtilityMethods.getBitmapFromDataStr(attachmentStr);
                                                    attachment.setBitmap(imgBitmap);
//                                                    updateMessage.getMessageAttachmentsList().add(imgBitmap);
                                                } else if (mimeType.equalsIgnoreCase("text/plain")) {
                                                    byte[] tmp2 = Base64.decode(attachmentStr, Base64.DEFAULT);
                                                    String text = null;
                                                    try {
                                                        text = new String(tmp2, "UTF-8");
                                                    } catch (UnsupportedEncodingException e) {
                                                        e.printStackTrace();
                                                    }
                                                    attachment.setAttachmentText(text);

//                                                    updateMessage.getTextAttachments().add(text);
                                                    Log.i("", text);

                                                } else {

                                                }
                                                updateMessage.getAttachmentArrayList().add(attachment);
                                            }
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                messagesAdapter.updateItem(finalIndex, updateMessage);
                                Log.i("", "");
                            }

                            @Override
                            public void onServerResultFailure(String failureMessage) {
                                Log.i("", "");
                            }
                        });
                    }
                }

            }

            @Override
            public void onServerResultFailure(String failureMessage) {
                binding.appBarChatting.chatLayout.swipeContainer.setRefreshing(false);
            }
        });

//
//        apiClient.getMessages(channel.getMessageIds(), channel.getId(), new ClientCallback() {
//            @Override
//            public void onServerResultSuccess(Object object) {
//                binding.appBarChatting.chatLayout.swipeContainer.setRefreshing(false);
//                Log.i("", "success");
//                JSONArray messagesJsonArray = (JSONArray) object;
//
//                final ArrayList<Message> messageList = getMessagesList(messagesJsonArray);
//
//                if (messagesAdapter == null) {
//                    initRecyclerView();
//                    messagesAdapter = new MessagesAdapter(messageList, ChattingActivity.this);
//                    binding.appBarChatting.chatLayout.chatRecyclerView.setAdapter(messagesAdapter);
//                } else
//                    messagesAdapter.setItems(messageList);
//
//                binding.appBarChatting.chatLayout.chatRecyclerView.scrollToPosition(messageList.size() - 1);
//
//
//                /*retrieve message attachment*/
//                //ToDo: to be changed with having position of messages having attachment
//                for (int index = 0; index < messageList.size(); index++) {
//                    Message message = messageList.get(index);
//                    if (message.getAttachments() != null && message.getAttachments().size() > 0) {
//                        final int finalIndex = index;
//                        apiClient.getAttachment(message.getAttachments(), new ClientCallback() {
//                            @Override
//                            public void onServerResultSuccess(Object object) {
//                                Message updateMessage = messageList.get(finalIndex);
//
//                                JSONArray attachmentsJsonArray = (JSONArray) object;
//                                for (int i = 0; i < attachmentsJsonArray.length(); i++) {
//                                    try {
//                                        JSONObject attachmentJsonObj = attachmentsJsonArray.getJSONObject(i);
//                                        String attachmentStr = attachmentJsonObj.optString("datas");
//                                        String mimeType = attachmentJsonObj.optString("mimetype");
//                                        if (attachmentStr != null && !"".equalsIgnoreCase(attachmentStr)) {
//                                            Attachment attachment = new Attachment();
//                                            attachment.setMimeType(mimeType);
//                                            attachment.setDatas(attachmentStr);
//                                            if (attachmentJsonObj.optString("url") != null)
//                                                attachment.setUrl(attachmentJsonObj.optString("url"));
//
//                                            if (attachmentJsonObj.optString("name") != null)
//                                                attachment.setName(attachmentJsonObj.optString("name"));
//
//
//                                            if (mimeType != null) {
//                                                if (mimeType.equalsIgnoreCase("image/jpeg")) {
//                                                    //get bitmap
//                                                    Bitmap imgBitmap = UtilityMethods.getBitmapFromDataStr(attachmentStr);
//                                                    attachment.setBitmap(imgBitmap);
////                                                    updateMessage.getMessageAttachmentsList().add(imgBitmap);
//                                                } else if (mimeType.equalsIgnoreCase("text/plain")) {
//                                                    byte[] tmp2 = Base64.decode(attachmentStr, Base64.DEFAULT);
//                                                    String text = null;
//                                                    try {
//                                                        text = new String(tmp2, "UTF-8");
//                                                    } catch (UnsupportedEncodingException e) {
//                                                        e.printStackTrace();
//                                                    }
//                                                    attachment.setAttachmentText(text);
//
////                                                    updateMessage.getTextAttachments().add(text);
//                                                    Log.i("", text);
//
//                                                } else {
//
//                                                }
//                                                updateMessage.getAttachmentArrayList().add(attachment);
//                                            }
//                                        }
//                                    } catch (JSONException e) {
//                                        e.printStackTrace();
//                                    }
//                                }
//                                messagesAdapter.updateItem(finalIndex, updateMessage);
//                                Log.i("", "");
//                            }
//
//                            @Override
//                            public void onServerResultFailure(String failureMessage) {
//                                Log.i("", "");
//                            }
//                        });
//                    }
//                }
//
//            }
//
//            @Override
//            public void onServerResultFailure(String failureMessage) {
//                binding.appBarChatting.chatLayout.swipeContainer.setRefreshing(false);
//            }
//        });
    }

    private ArrayList<Message> getMessagesList(JSONArray messagesJsonArray) {
        ArrayList<Message> messageList = new ArrayList<>();
        Gson gson = VolleyHelper.customizeGsonBuilder().create();
        JSONArray usersJsonArray = (JSONArray) messagesJsonArray;
        if (usersJsonArray != null) {

            Type collectionType = new TypeToken<List<Message>>() {
            }.getType();
            messageList = gson.fromJson(messagesJsonArray.toString(), collectionType);
        }
        return messageList;
    }

    private void initRecyclerView() {
        StaggeredGridLayoutManager recyclerViewLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        binding.appBarChatting.chatLayout.chatRecyclerView.setLayoutManager(recyclerViewLayoutManager);

        try {
            binding.appBarChatting.chatLayout.chatRecyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                @Override
                public void onLayoutChange(View v,
                                           int left, int top, int right, int bottom,
                                           int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    try {
                        if (bottom < oldBottom) {
                            binding.appBarChatting.chatLayout.chatRecyclerView.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    binding.appBarChatting.chatLayout.chatRecyclerView.smoothScrollToPosition(
                                            binding.appBarChatting.chatLayout.chatRecyclerView.getAdapter().getItemCount() - 1);
                                }
                            }, 100);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onSendMessageClicked(View view) {
        String messageText = binding.appBarChatting.chatLayout.messageEditText.getText().toString();
        if (messageText != null && !messageText.trim().isEmpty() && selectedChannelId != 0) {
            String messageBody = binding.appBarChatting.chatLayout.messageEditText.getText().toString();
            binding.appBarChatting.chatLayout.messageEditText.setText("");
            apiClient.sendMessage(messageBody, selectedChannelId, new ClientCallback() {
                @Override
                public void onServerResultSuccess(Object object) {
                    int messageId = (int) object;
                    addMessageToRecyclerView(messageId);
                }

                @Override
                public void onServerResultFailure(String failureMessage) {
                    Log.i("", "");
                }
            });
        }
    }


    private void addMessageToRecyclerView(int messageId) {
        ArrayList<Integer> messagesIds = new ArrayList<>();
        messagesIds.add(messageId);

        apiClient.getUserMessages(messagesIds, new ClientCallback() {
            @Override
            public void onServerResultSuccess(Object object) {
                ArrayList<Message> messageList = getMessagesList((JSONArray) object);
                if (messagesAdapter != null) {
                    messagesAdapter.addItem(messageList.get(0));
                    binding.appBarChatting.chatLayout.chatRecyclerView.scrollToPosition(messagesAdapter.getItems().size() - 1);
                }
            }

            @Override
            public void onServerResultFailure(String failureMessage) {
                Log.i("", "");
            }
        });
    }

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {

        }
    }

    private Bitmap decodeFile(File f) {
        Log.d("file decodig ", "..." + f.getAbsolutePath());
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            FileInputStream is = new FileInputStream(f);
            Bitmap decodedBitmap = BitmapFactory.decodeStream(is, null, o);
            is.close();

            final int REQUIRED_SIZE = 560;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            is = new FileInputStream(f);
            decodedBitmap = BitmapFactory.decodeStream(is, null, o2);
            is.close();
            return decodedBitmap;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @SuppressLint("NewApi")
    private String getPath(Uri uri) {
        String path = "";
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            path = RealPathUtil.getRealPathFromURI_API11to18(this, uri);
        } else {
            path = RealPathUtil.getRealPathFromURI_API19(this, uri);
        }

        return path;
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;

        Cursor cursor = getApplicationContext().getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            if (idx > -1)
                result = cursor.getString(idx);
            else
                result = null;

            cursor.close();
        }
        return result;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /*if image attachment, get attached bitmap*/
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                final boolean isCamera;
                if (data == null) {
                    isCamera = true;
                } else {
                    final String action = data.getAction();
                    if (action == null) {
                        isCamera = false;
                    } else {
                        isCamera = action.equals("inline data");
                    }
                }

                Uri selectedImageUri;

                if (isCamera) {
                    selectedImageUri = UtilityMethods.outputFileUri;
                } else {
                    selectedImageUri = data == null ? null : data.getData();
                }
                if (selectedImageUri == null) {
                    UtilityMethods.showSnackbar(getResources().getString(R.string.avatar_error), binding.getRoot(), this);
                } else {

                    // TODO Refactore the get path part to handle differnet versions better
                    // check if the there is a path only
                    String selectedImagePath = getPath(selectedImageUri);

                    if (selectedImagePath != null) {
                        // gets path from devices less >= LOLIPOP
                        selectedImagePath = getRealPathFromURI(selectedImageUri);

                        File file = null;

                        if (selectedImagePath != null)
                            file = new File(selectedImagePath);

                        else
                            file = new File(getPath(selectedImageUri));

                        Bitmap sourceBitmap = decodeFile(file);
//                        Bitmap sourceBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                        int size = Integer.parseInt(String.valueOf(file.length() / 1024));
                        String mimeType = UtilityMethods.getMimeType(selectedImagePath);
                        String filename = file.getName();
                        String base64 = UtilityMethods.getEncodedImage(sourceBitmap);


                        Attachment attachment = new Attachment(filename, "ir.attachment", base64, mimeType);
                        try {
                            attachment.setFileSize(size);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        attachment.setResId(selectedChannelId);
                        apiClient.createAttachment(attachment, new ClientCallback() {
                            @Override
                            public void onServerResultSuccess(Object object) {
                                try {
                                    final int savedAttachmentId = (int) object;

                                    apiClient.getAttachment(savedAttachmentId, new ClientCallback() {
                                        @Override
                                        public void onServerResultSuccess(Object object) {
                                            ArrayList<Integer> attachmentIdArrayList = new ArrayList<Integer>();
                                            attachmentIdArrayList.add(savedAttachmentId);
                                            apiClient.createMessage(attachmentIdArrayList, selectedChannelId, new ClientCallback() {
                                                @Override
                                                public void onServerResultSuccess(Object object) {
                                                    int messageId = (int) object;
                                                    addMessageToRecyclerView(messageId);
                                                }

                                                @Override
                                                public void onServerResultFailure(String failureMessage) {
                                                    Log.i("", "message creation failed");
                                                }
                                            });
                                        }

                                        @Override
                                        public void onServerResultFailure(String failureMessage) {
                                            Log.i("", "");
                                        }
                                    });

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onServerResultFailure(String failureMessage) {
                                Log.i("", "");
                            }
                        });
                    }
                }
            }
        }


    }


    public String getDataColumn(Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};
        try {
            cursor = getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    private byte[] copyFileStream(File dest, Uri uri)
            throws IOException {
        byte[] buffer = new byte[1024];
        InputStream is = null;
        OutputStream os = null;
        try {
            is = getContentResolver().openInputStream(uri);
            os = new FileOutputStream(dest);

            int length;

            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);

            }
            is.close();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return buffer;
    }

    private String encodeFileToBase64Binary(File file)
            throws IOException {

        byte[] bytes = loadFile(file);
//        byte[] encoded = Base64.encodeBase64(bytes);
        String encodedString = Base64.encodeToString(bytes, Base64.DEFAULT);

        return encodedString;
    }

    private static byte[] loadFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }
        byte[] bytes = new byte[(int) length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }

        is.close();
        return bytes;
    }

    public void onAttachButtonClicked(View view) {
        if (hasPermission(Manifest.permission.CAMERA)) {
            if (hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE))
                startCameraIntent();
            else
                requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE, getString(R.string.request_read_storage_permission), REQUEST_STORAGE_CODE);
        } else
            requestPermission(Manifest.permission.CAMERA, getString(R.string.request_camera_permission), REQUEST_CAMERA_CODE);
    }
}
