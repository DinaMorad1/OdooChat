package byteis.com.odoochat.ui;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.util.LogWriter;
import android.util.Log;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import byteis.com.odooapilibrary.api.ClientCallback;
import byteis.com.odooapilibrary.managers.EncryptionManager;
import byteis.com.odoochat.R;
import byteis.com.odoochat.adapter.DatabasesSpinnerAdapter;
import byteis.com.odoochat.data.SharedPref;
import byteis.com.odoochat.databinding.ActivityLoginBinding;
import byteis.com.odoochat.util.UtilityMethods;

public class LoginActivity extends BaseActivity implements ClientCallback, View.OnFocusChangeListener {

    private ActivityLoginBinding binding;
    private boolean isValidRemoteUrl = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_login, null, false);
        inflateContent(binding.getRoot());
        setToolBarContent("Odoo Login", true, false);
        binding.urlEdittext.setOnFocusChangeListener(this);
    }

    public void onLoginClicked(View view) {
        resetValidatinErrors();
        if (isValidData()) {
            if (isValidRemoteUrl) {
                String url = binding.urlEdittext.getText().toString();

                if (!url.contains("http") && !url.contains("https"))
                    url = "http://" + url;

                byteis.com.odooapilibrary.data.SharedPref.SaveString(this, byteis.com.odooapilibrary.data.SharedPref.REMOTE_URL, url);
                showProgressDialog(getString(R.string.please_wait), false, true);
                apiClient.odooLogin(binding.usernameEdittext.getText().toString(), binding.password.getText().toString(), binding.spinnerDatabaseList.getSelectedItem().toString(), this);
            } else
                UtilityMethods.showSnackbar(getString(R.string.invalid_url), binding.getRoot(), this);
        }
    }

    private void resetValidatinErrors() {
        setError(binding.usernameTextInput, null);
        setError(binding.passwordTextInput, null);
    }

    private boolean isValidData() {
        boolean isValid = true;
        if (binding.usernameEdittext.getText().toString().trim().isEmpty()) {
            setError(binding.usernameTextInput, getString(R.string.invalid_username));
            isValid = false;
        }

        if (binding.password.getText().toString().trim().isEmpty()) {
            setError(binding.passwordTextInput, getString(R.string.invalid_password));
            isValid = false;
        }

        return isValid;
    }

    @Override
    public void onServerResultSuccess(Object object) {
        if (object != null) {
            final int uid = Integer.parseInt(object.toString());
            if (uid != 0) {


                SharedPref.SaveInt(LoginActivity.this, SharedPref.USER_ID, uid);

                    /*Saving encrypted user password -- todo try to make this with ndk*/
                if (binding.password.getText() != null) {
                    String passwordText = binding.password.getText().toString();
                    EncryptionManager.savePassword(passwordText, LoginActivity.this);
                }

                    /*save database*/
                String databaseName = binding.spinnerDatabaseList.getSelectedItem().toString();
                byteis.com.odooapilibrary.data.SharedPref.SaveString(LoginActivity.this, byteis.com.odooapilibrary.data.SharedPref.DATABASE_NAME, databaseName);


                apiClient.getUser(new ClientCallback() {
                                      @Override
                                      public void onServerResultSuccess(Object object) {
                                          JSONArray userArray = (JSONArray) object;
                                          try {
                                              JSONObject userJsonObj = userArray.getJSONObject(0);
                                              String userName = userJsonObj.optString("display_name");
                                              SharedPref.SaveString(LoginActivity.this, SharedPref.USER_NAME, userName);
                                              String userImg = userJsonObj.optString("image");
                                              SharedPref.SaveString(LoginActivity.this, SharedPref.USER_IMAGE, userImg);
                                          } catch (JSONException e) {
                                              e.printStackTrace();
                                          }


                                          if (!isFinishing()) {
                                              ActivityCompat.finishAffinity(LoginActivity.this);
                                              startActivityWithTransition(LoginActivity.this, new Intent(LoginActivity.this, ChattingActivity.class));
                                          }
                                      }

                                      @Override
                                      public void onServerResultFailure(String failureMessage) {

                                      }
                                  }, uid
                );

//                    SharedPref.SaveString(this, SharedPref.SESSION_ID, jsonObject.getString("session_id"));

            } else
                serverFailure(getString(R.string.something_wrong));
        }
    }

    private void serverFailure(String failureMessage) {
        SharedPref.deleteShared(this);
        if (!isFinishing()) {
            dismissProgressDialog();
            UtilityMethods.showSnackbar(failureMessage, binding.getRoot(), LoginActivity.this);
        }
    }

    @Override
    public void onServerResultFailure(String failureMessage) {
        serverFailure(failureMessage);

    }

    @Override
    public void onFocusChange(View view, final boolean b) {
        if (!b) {/*unfocused view*/
            setError(binding.urlTextInput, null);
            String url = binding.urlEdittext.getText().toString();

            apiClient.getDatabases(new ClientCallback() {
                @Override
                public void onServerResultSuccess(Object object) {
                    isValidRemoteUrl = true;
                    Log.i("", "success");
                    //fill the spinner with databases list
                    JSONArray jsonArray = (JSONArray) object;
                    String[] stringsArray = new String[jsonArray.length()];
                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            stringsArray[i] = jsonArray.getString(i);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    binding.spinnerDatabaseList.setVisibility(View.VISIBLE);
                    binding.spinnerSeparator.setVisibility(View.VISIBLE);
                    DatabasesSpinnerAdapter spinnerAdapter = new DatabasesSpinnerAdapter(LoginActivity.this, R.layout.spinner_databases_label, stringsArray);
                    binding.spinnerDatabaseList.setAdapter(spinnerAdapter);
                }

                @Override
                public void onServerResultFailure(String failureMessage) {
                    isValidRemoteUrl = false;
                    setError(binding.urlTextInput, getString(R.string.invalid_url));
                }
            }, binding.urlEdittext.getText().toString());

        }
    }
}
