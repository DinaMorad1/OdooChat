package byteis.com.odoochat.models;

import com.google.gson.annotations.SerializedName;
import com.google.gson.internal.LinkedTreeMap;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by Dina on 14/11/2016.
 */

public class Channel {


    public final static String DIRECT_MESSAGE_TYPE = "channel_direct_message";
    public final static String PUBLIC_CHANNEL_TYPE = "channel_channel";
    public final static String PRIVATE_CHANNEL_TYPE = "channel_private_group";

    @SerializedName("id")
    private int id;

    //    @SerializedName("display_name")
    private String displayName;

//    @SerializedName("message_ids")
//    private ArrayList messageIds;//channel_message_ids

    @SerializedName("direct_partner")
    private ArrayList channelPartnerIds;

    public String getChType() {
        return chType;
    }

    public void setChType(String chType) {
        this.chType = chType;
    }

    private String chType;

//    private boolean isPrivate;
//
//    private boolean isDirectMessage;

//    public boolean isPrivate() {
//        return isPrivate;
//    }

//    public void setPrivate(boolean aPrivate) {
//        isPrivate = aPrivate;
//    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

//    public ArrayList getMessageIds() {
//        return messageIds;
//    }
//
//    public void setMessageIds(ArrayList messageIds) {
//        this.messageIds = messageIds;
//    }

//    public boolean isDirectMessage() {
//        return isDirectMessage;
//    }

//    public void setDirectMessage(boolean directMessage) {
//        isDirectMessage = directMessage;
//    }

    public ArrayList getChannelPartnerIds() {
        return channelPartnerIds;
    }

    public void setChannelPartnerIds(ArrayList channelPartnerIds) {
        this.channelPartnerIds = channelPartnerIds;
    }

    public static ArrayList<Channel> fromJson(JSONArray channelsJsonArray, String channelType) {
        ArrayList<Channel> chsList = new ArrayList<>();
        Channel channel;
        JSONArray directPartnerJsonArray;
        JSONObject channelJsonObj;
        JSONObject partnerJsonObj;
        if (channelsJsonArray != null) {
            for (int i = 0; i < channelsJsonArray.length(); i++) {
                channel = new Channel();
                channelJsonObj = channelsJsonArray.optJSONObject(i);
                if (channelJsonObj != null) {
                    directPartnerJsonArray = channelJsonObj.optJSONArray("direct_partner");
                    if (directPartnerJsonArray != null && directPartnerJsonArray.length() > 0) {
                        partnerJsonObj = directPartnerJsonArray.optJSONObject(0);
                        if (partnerJsonObj != null)
                            channel.setDisplayName(partnerJsonObj.optString("name"));
                    } else
                        channel.setDisplayName(channelJsonObj.optString("name"));
                    channel.setId(channelJsonObj.optInt("id"));
                    channel.setChType(channelType);
                    chsList.add(channel);
                }
            }
        }
        return chsList;
    }

}
