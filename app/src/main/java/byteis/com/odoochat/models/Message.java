package byteis.com.odoochat.models;

import android.graphics.Bitmap;

import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Dina on 13/11/2016.
 */

public class Message {


    //    @SerializedName("id")
    private int serverId;

    @SerializedName("body")
    private String text;

    @SerializedName("author_avatar")
    private String avatar;

    @SerializedName("author_id")
    private ArrayList fromArrayList;

    @SerializedName("write_date")
    private String writeDate;

    @SerializedName("attachment_ids")
    private ArrayList<Integer> attachments;

    @SerializedName("tracking_value_ids")
    private ArrayList<Integer> trackingCodes;

    private ArrayList<Bitmap> messageAttachmentsList = new ArrayList<>();

    private ArrayList<String> textAttachments = new ArrayList<>();

    private ArrayList<Attachment> attachmentArrayList = new ArrayList<>();

    private int fromId;

    private String fromName;

//    @SerializedName("from_id")
//    private JSONArray from;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }


    public int getServerId() {
        return serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getFromName() {
        if (fromArrayList != null && fromArrayList.size() > 1)
            return (String) fromArrayList.get(1);
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public int getFromId() {
        return fromId;
    }

    public void setFromId(int fromId) {
        this.fromId = fromId;
    }

    public String getWriteDate() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = format.parse(writeDate);

            System.out.println(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return writeDate;
    }

    public Long getWriteDateLong() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long retDateLong = 0;
        try {
            Date date = format.parse(writeDate);
            retDateLong = date.getTime();
            System.out.println(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return retDateLong;
    }

    public void setWriteDate(String writeDate) {
        this.writeDate = writeDate;
    }

    public ArrayList<Integer> getAttachments() {
        return attachments;
    }

    public void setAttachments(ArrayList<Integer> attachments) {
        this.attachments = attachments;
    }


    public ArrayList<Bitmap> getMessageAttachmentsList() {
        return messageAttachmentsList;
    }

    public void setMessageAttachmentsList(ArrayList<Bitmap> messageAttachmentsList) {
        this.messageAttachmentsList = messageAttachmentsList;
    }

    public ArrayList<String> getTextAttachments() {
        return textAttachments;
    }

    public void setTextAttachments(ArrayList<String> textAttachments) {
        this.textAttachments = textAttachments;
    }

    public ArrayList<Attachment> getAttachmentArrayList() {
        return attachmentArrayList;
    }

    public void setAttachmentArrayList(ArrayList<Attachment> attachmentArrayList) {
        this.attachmentArrayList = attachmentArrayList;
    }

    public ArrayList<Integer> getTrackingCodes() {
        return trackingCodes;
    }

    public void setTrackingCodes(ArrayList<Integer> trackingCodes) {
        this.trackingCodes = trackingCodes;
    }


}
