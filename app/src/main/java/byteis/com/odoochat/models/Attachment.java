package byteis.com.odoochat.models;


import android.graphics.Bitmap;

public class Attachment {

    public final static String IMAGE_TYPE = "image/jpeg";
    public final static String TEXT_TYPE = "text/plain";

    public Attachment(String name, String modelName, String datas, String mimeType) {
        this.name = name;
        this.modelName = modelName;
        this.datas = datas;
        this.mimeType = mimeType;
    }


    public Attachment() {
    }

    private String name;

    private String datas;

    private String mimeType;

    private String resModel;

    private String datasFname;

    private String modelName;

    private int fileSize;

    private Bitmap bitmap;

    private String attachmentText;

    private String url;

    private int resId;

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    private String channelName;

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public String getAttachmentText() {
        return attachmentText;
    }

    public void setAttachmentText(String attachmentText) {
        this.attachmentText = attachmentText;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getFileSize() {
        return fileSize;
    }

    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    public String getDatasFname() {
        return datasFname;
    }

    public void setDatasFname(String datasFname) {
        this.datasFname = datasFname;
    }

    public String getResModel() {
        return resModel;
    }

    public void setResModel(String resModel) {
        this.resModel = resModel;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getDatas() {
        return datas;
    }

    public void setDatas(String datas) {
        this.datas = datas;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }
}
