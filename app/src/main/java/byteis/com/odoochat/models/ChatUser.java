package byteis.com.odoochat.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Dina on 14/11/2016.
 */

public class ChatUser {

    @SerializedName("display_name")
    private String name;

    @SerializedName("image")
    private String image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
