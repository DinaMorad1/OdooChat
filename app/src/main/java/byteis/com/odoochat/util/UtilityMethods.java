package byteis.com.odoochat.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.text.format.DateFormat;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Calendar;
import java.util.concurrent.atomic.AtomicInteger;

import byteis.com.odoochat.R;
import byteis.com.odoochat.listeners.KeypadVisibilityListener;


/**
 * Created by Dina on 09/10/2016.
 */

public class UtilityMethods {

    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);
    public static Uri outputFileUri;


    public static boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static Snackbar showSnackbar(final String snackbarMessage, View contentView, Context context) {
        Snackbar snackbar = Snackbar.make(contentView, snackbarMessage, Snackbar.LENGTH_SHORT);
        styleSnackbar(snackbar, context);
        hideKeypad(context);
        snackbar.show();
        return snackbar;
    }

    private static void styleSnackbar(Snackbar snackbar, Context context) {
        ViewGroup group = (ViewGroup) snackbar.getView();
        group.setBackgroundColor(ActivityCompat.getColor(context, R.color.primary_color));
        snackbar.setActionTextColor(Color.parseColor("#E6E6E6"));
        TextView actionTextView = (TextView) group.findViewById(R.id.snackbar_action);
        actionTextView.setTextSize(18);
        actionTextView.setTypeface(null, Typeface.BOLD);
        TextView tv = (TextView) group.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
    }

    private static void hideKeypad(Context context) {
        View view = ((Activity) context).getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    public static int generateViewId() {
        for (; ; ) {
            final int result = sNextGeneratedId.get();
            // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
            int newValue = result + 1;
            if (newValue > 0x00FFFFFF) newValue = 1; // Roll over to 1, not 0.
            if (sNextGeneratedId.compareAndSet(result, newValue)) {
                return result;
            }
        }
    }

    public static Bitmap scaleBitmap(Bitmap bitmapToScale, float newWidth, float newHeight) {
        if (bitmapToScale == null)
            return null;
        int width = bitmapToScale.getWidth();
        int height = bitmapToScale.getHeight();
        Matrix matrix = new Matrix();
        matrix.postScale(newWidth / width, newHeight / height);
        Bitmap scaledBitmap = Bitmap.createBitmap(bitmapToScale, 0, 0, bitmapToScale.getWidth(), bitmapToScale.getHeight(), matrix, true);
        return scaledBitmap;
    }

    public static String getFormattedDate(Context context, long smsTimeInMilis) {
        Calendar smsTime = Calendar.getInstance();
        smsTime.setTimeInMillis(smsTimeInMilis);

        Calendar now = Calendar.getInstance();

        final String timeFormatString = "h:mm aa";
        final String dateTimeFormatString = "EEEE, MMMM d, h:mm aa";
        final long HOURS = 60 * 60 * 60;
        if (now.get(Calendar.DATE) == smsTime.get(Calendar.DATE)) {
            return "Today " + DateFormat.format(timeFormatString, smsTime);
        } else if (now.get(Calendar.DATE) - smsTime.get(Calendar.DATE) == 1) {
            return "Yesterday " + DateFormat.format(timeFormatString, smsTime);
        } else if (now.get(Calendar.YEAR) == smsTime.get(Calendar.YEAR)) {
            return DateFormat.format(dateTimeFormatString, smsTime).toString();
        } else {
            return DateFormat.format("MMMM dd yyyy, h:mm aa", smsTime).toString();
        }
    }

    public static Bitmap getRoundedBitmap(Bitmap bitmap, int widthImage, int heightImage) {

        Bitmap resized = Bitmap.createScaledBitmap(bitmap, widthImage, heightImage, true);
        Bitmap result = Bitmap.createBitmap(widthImage, heightImage, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(result);
        Paint paint = new Paint();

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawCircle(widthImage / 2, heightImage / 2, widthImage / 2, paint);

        // Mode.SRC_IN: Only the part of the source image that overlaps the destination image will be drawn.
        // REF: http://www.ibm.com/developerworks/java/library/j-mer0918/
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        Rect rect = new Rect(0, 0, widthImage, heightImage);
        canvas.drawBitmap(resized, rect, rect, paint);

        return result;
    }

    public static Bitmap getBitmapFromDataStr(String dataStr) {
        byte[] decodedString = Base64.decode(dataStr, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }

    public static int getBitmapWidth(Bitmap source, int targetHeight) {
        int targetWidth = 0;
        if (source != null) {
            double aspectRatio = (double) source.getWidth() / (double) source.getHeight();
            targetWidth = (int) (targetHeight * aspectRatio);
        }
        return targetWidth;
    }

    public static void setKeyboardVisibilityListener(Activity activity, final KeypadVisibilityListener keyboardVisibilityListener) {
        try {
            final View contentView = activity.findViewById(android.R.id.content);
            contentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                private int mPreviousHeight;

                @Override
                public void onGlobalLayout() {
                    int newHeight = contentView.getHeight();
                    if (mPreviousHeight != 0) {
                        if (mPreviousHeight > newHeight) {
                            // Height decreased: keyboard was shown
                            keyboardVisibilityListener.onKeyboardVisibilityChanged(true);
                        } else if (mPreviousHeight < newHeight) {
                            // Height increased: keyboard was hidden
                            keyboardVisibilityListener.onKeyboardVisibilityChanged(false);
                        } else {
                            // No change
                        }
                    }
                    mPreviousHeight = newHeight;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void createImage(Context context) {
        final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "MyDir" + File.separator);
        root.mkdirs();

        final String fname = System.currentTimeMillis() + ".jpg";
        final File sdImageMainDirectory = new File(root, fname);
        outputFileUri = Uri.fromFile(sdImageMainDirectory);
        sdImageMainDirectory.deleteOnExit();
    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    public static String getEncodedImage(Bitmap bitmap) {
        String encodedImage = null;
        if (bitmap != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, baos);
            byte[] b = baos.toByteArray();
            encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        }
        return encodedImage;
    }

    public static Snackbar getSnackbar(final String snackbarMessage, View contentView, Context context) {
        Snackbar snackbar = Snackbar.make(contentView, snackbarMessage, Snackbar.LENGTH_SHORT);
        styleSnackbar(snackbar, context);
        hideKeypad(context);
        return snackbar;
    }

}
