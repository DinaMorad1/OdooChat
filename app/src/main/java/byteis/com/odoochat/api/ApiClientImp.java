package byteis.com.odoochat.api;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import byteis.com.odooapilibrary.api.ClientCallback;
import byteis.com.odooapilibrary.data.SharedPref;
import byteis.com.odoochat.models.Attachment;

/**
 * Created by Dina on 13/11/2016.
 */

public class ApiClientImp extends byteis.com.odooapilibrary.api.ApiClientImp implements ApiClient {

    private static ApiClientImp instance;

    private ApiClientImp(Context context) {
        super(context);
    }

    public static ApiClientImp getInstance(Context context) {
        if (instance == null)
            instance = new ApiClientImp(context);
        return instance;
    }

    private String getRemoteUrl() {
        return SharedPref.LoadString(context, SharedPref.REMOTE_URL);
    }

    @Override
    public void getUserSessions(int uid, final ClientCallback callback) {
        String url = getRemoteUrl() + ODOO_JSON_RPC;

         /*search criteria filter*/
        JSONArray methodParams = new JSONArray();
        JSONArray paramsArray = new JSONArray();

        /*filter to render parameters with confirmed state*/
        JSONArray inJsonArray = new JSONArray();
        inJsonArray.put(uid);

        JSONArray itemsJsonArray = new JSONArray();
        itemsJsonArray.put("user_ids");
        itemsJsonArray.put("in");
        itemsJsonArray.put(inJsonArray);
        paramsArray.put(itemsJsonArray);

        itemsJsonArray = new JSONArray();
        itemsJsonArray.put("user_ids");
        itemsJsonArray.put("in");
        inJsonArray = new JSONArray();
        int loginUserId = SharedPref.LoadInt(context, SharedPref.USER_ID);//get session between the currently login user and the selected user
        inJsonArray.put(loginUserId);
        itemsJsonArray.put(inJsonArray);
        paramsArray.put(itemsJsonArray);

        methodParams.put(paramsArray);

        /*needed fields to be returned from the model*/
        JSONArray showItems = new JSONArray();
        boolean limit = false;
        int offset = 0;

        searchRead(methodParams, showItems, url, limit, offset, "im_chat.session"//"res.users"//"im_chat.message"
                , new ClientCallback() {
                    @Override
                    public void onServerResultSuccess(Object object) {
                        JSONArray messagesIds = new JSONArray();
                        JSONArray sessionsJsonArray = (JSONArray) object;

                        for (int i = 0; i < sessionsJsonArray.length(); i++) {
                            JSONObject sessionJsonObj = sessionsJsonArray.optJSONObject(i);
                            JSONArray messagesJsonArray = sessionJsonObj.optJSONArray("message_ids");
                            for (int j = 0; j < messagesJsonArray.length(); j++) {
                                messagesIds.put(messagesJsonArray.optInt(j));
                            }
                        }
                        getMessages(messagesIds, callback);
                    }

                    @Override
                    public void onServerResultFailure(String failureMessage) {
                        callback.onServerResultFailure(failureMessage);
                    }
                }, JSONObject.class);
    }

    public void getMessages(JSONArray messagesIds, ClientCallback callback) {
        String url = getRemoteUrl() + ODOO_JSON_RPC;

         /*search criteria filter*/
        JSONArray methodParams = new JSONArray();
        JSONArray paramsArray = new JSONArray();

        JSONArray itemsJsonArray = new JSONArray();
        itemsJsonArray.put("id");
        itemsJsonArray.put("in");
        itemsJsonArray.put(messagesIds);
        paramsArray.put(itemsJsonArray);

        methodParams.put(paramsArray);

        /*needed fields to be returned from the model*/
        JSONArray showItems = new JSONArray();
        boolean limit = false;
        int offset = 0;

        searchRead(methodParams, showItems, url, limit, offset, "im_chat.message", callback, JSONObject.class);
    }

    @Override
    public void getUsers(ClientCallback callback) {
        String url = getRemoteUrl() + ODOO_JSON_RPC;

         /*search criteria filter*/
        JSONArray methodParams = new JSONArray();
        JSONArray paramsArray = new JSONArray();

//        JSONArray itemsJsonArray = new JSONArray();
//        itemsJsonArray.put("id");
//        itemsJsonArray.put("!=");
//        itemsJsonArray.put(SharedPref.LoadInt(context, SharedPref.USER_ID));
//        paramsArray.put(itemsJsonArray);

        methodParams.put(paramsArray);

        /*needed fields to be returned from the model*/
        JSONArray showItems = new JSONArray();
        boolean limit = false;
        int offset = 0;

        searchRead(methodParams, showItems, url, limit, offset, "res.users", callback, JSONObject.class);
    }

    @Override
    public void getUser(ClientCallback callback, int userId) {
        String url = getRemoteUrl() + ODOO_JSON_RPC;

         /*search criteria filter*/
        JSONArray methodParams = new JSONArray();
        JSONArray paramsArray = new JSONArray();

        JSONArray itemsJsonArray = new JSONArray();
        itemsJsonArray.put("id");
        itemsJsonArray.put("=");
        itemsJsonArray.put(userId);
        paramsArray.put(itemsJsonArray);

        methodParams.put(paramsArray);

        /*needed fields to be returned from the model*/
        JSONArray showItems = new JSONArray();
        boolean limit = false;
        int offset = 0;

        searchRead(methodParams, showItems, url, limit, offset, "res.users", callback, JSONObject.class);
    }

    @Override
    public void getChannels(ClientCallback callback, String channelType, String privateState, boolean isMember) {
        String url = getRemoteUrl() + ODOO_JSON_RPC;

         /*search criteria filter*/
        JSONArray methodParams = new JSONArray();
        JSONArray paramsArray = new JSONArray();

        JSONArray itemsJsonArray = new JSONArray();
        paramsArray.put("|");
        itemsJsonArray.put("channel_type");
        itemsJsonArray.put("=");
        itemsJsonArray.put(channelType);
        paramsArray.put(itemsJsonArray);
        itemsJsonArray = new JSONArray();
        itemsJsonArray.put("channel_type");
        itemsJsonArray.put("=");
        itemsJsonArray.put("chat");
        paramsArray.put(itemsJsonArray);

        itemsJsonArray = new JSONArray();
        paramsArray.put("|");
        itemsJsonArray.put("public");
        itemsJsonArray.put("=");
        itemsJsonArray.put(privateState);
        paramsArray.put(itemsJsonArray);
        itemsJsonArray = new JSONArray();
        itemsJsonArray.put("public");
        itemsJsonArray.put("=");
        itemsJsonArray.put("private");
        paramsArray.put(itemsJsonArray);


        if (isMember) {//todo: check why this filter is ignored
            itemsJsonArray = new JSONArray();
            itemsJsonArray.put("is_member");
            itemsJsonArray.put("=");
            itemsJsonArray.put(isMember ? 1 : 0);
            paramsArray.put(itemsJsonArray);
        }

        methodParams.put(paramsArray);

        /*needed fields to be returned from the model*/
        JSONArray showItems = new JSONArray();
        boolean limit = false;
        int offset = 0;

        searchRead(methodParams, showItems, url, limit, offset, "mail.channel", callback, JSONObject.class);
    }

    @Override
    public void getPartners(ArrayList<Integer> partnerIds, ClientCallback callback) {
        String url = getRemoteUrl() + ODOO_JSON_RPC;

         /*search criteria filter*/
        JSONArray methodParams = new JSONArray();
        JSONArray paramsArray = new JSONArray();

        JSONArray itemsJsonArray = new JSONArray();

        JSONArray inJsonArray = new JSONArray(partnerIds);
        itemsJsonArray.put("id");
        itemsJsonArray.put("in");
        itemsJsonArray.put(inJsonArray);
        paramsArray.put(itemsJsonArray);


        methodParams.put(paramsArray);

        /*needed fields to be returned from the model*/
        JSONArray showItems = new JSONArray();
        boolean limit = false;
        int offset = 0;

        searchRead(methodParams, showItems, url, limit, offset, "res.partner", callback, JSONObject.class);
    }

    @Override
    public void sendMessage(String messageBody, int selectedChannelId, ClientCallback callback) {
        String url = getRemoteUrl() + ODOO_JSON_RPC;

        JSONObject kw = new JSONObject();
        try {
            kw.put("body", messageBody);
            kw.put("message_type", "comment");
            kw.put("content_subtype", "html");
            kw.put("subtype", "mail.mt_comment");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONArray methodParamsJsonArray = new JSONArray();
        methodParamsJsonArray.put(selectedChannelId);
        String method = "message_post";

        post(kw, methodParamsJsonArray, method, url, "mail.channel", callback, JSONObject.class);
    }

    @Override
    public void createMessage(ArrayList<Integer> attachmentIds, int channelId, ClientCallback callback) {
        String url = getRemoteUrl() + ODOO_JSON_RPC;
        JSONArray attachmentIdsJsonArray = new JSONArray(attachmentIds);
        JSONObject kw = new JSONObject();
        try {
            kw.put("attachment_ids", attachmentIdsJsonArray);
            kw.put("message_type", "comment");
            kw.put("content_subtype", "html");
            kw.put("subtype", "mail.mt_comment");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONArray methodParamsJsonArray = new JSONArray();
        methodParamsJsonArray.put(channelId);
        String method = "message_post";

        post(kw, methodParamsJsonArray, method, url, "mail.channel", callback, JSONObject.class);

    }

    @Override
    public void createAttachment(Attachment attachment, ClientCallback callback) {
        String url = getRemoteUrl() + ODOO_JSON_RPC;

        JSONObject kw = new JSONObject();
        try {
            kw.put("mimetype", attachment.getMimeType());
            kw.put("res_model", attachment.getModelName());
            kw.put("name", attachment.getName());
            kw.put("datas", attachment.getDatas());
            kw.put("res_id", attachment.getResId());
            kw.put("datas_fname", attachment.getName());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        postModel(kw, CREATE_METHOD, url, ATTACHMENT_MODEL
                , callback, JSONObject.class);
    }


    @Override
    public void getFilteredChannels(ClientCallback callback) {
        String url = getRemoteUrl() + ODOO_JSON_RPC;



         /*search criteria filter*/
        JSONArray methodParams = new JSONArray();
        JSONArray paramsArray = new JSONArray();

        methodParams.put(paramsArray);

        /*needed fields to be returned from the model*/
        JSONArray showItems = new JSONArray();
        boolean limit = false;
        int offset = 0;

        invokeMethod("channel_fetch_slot", methodParams, showItems, url, limit, offset, "mail.channel", callback, JSONObject.class);
    }

    @Override
    public void getMessages(ArrayList messageIds, int channelId, ClientCallback callback) {
        String url = getRemoteUrl() + ODOO_JSON_RPC;

         /*search criteria filter*/
        JSONArray methodParams = new JSONArray();
        JSONArray paramsArray = new JSONArray();

        JSONArray itemsJsonArray = new JSONArray();
        itemsJsonArray.put("id");
        itemsJsonArray.put("in");

        JSONArray idsJsonArray = new JSONArray();

        for (int i = 0; i < messageIds.size(); i++) {
            String messageIdStr = messageIds.get(i).toString();
            double val = Double.parseDouble(messageIdStr);
            int intVal = (int) val;
            idsJsonArray.put(intVal);
        }

        itemsJsonArray.put(idsJsonArray);
        paramsArray.put(itemsJsonArray);

        /*filtering if message belongs to this channel*/
        itemsJsonArray = new JSONArray();
        itemsJsonArray.put("channel_ids");
        itemsJsonArray.put("in");
        itemsJsonArray.put(channelId);
        paramsArray.put(itemsJsonArray);

        methodParams.put(paramsArray);

        /*needed fields to be returned from the model*/
        JSONArray showItems = new JSONArray();
        boolean limit = false;
        int offset = 0;

        searchRead(methodParams, showItems, url, limit, offset, "id asc", "mail.message", callback, JSONObject.class);
    }

    @Override
    public void getAttachment(ArrayList<Integer> attachmentIds, ClientCallback callback) {
        String url = getRemoteUrl() + ODOO_JSON_RPC;

         /*search criteria filter*/
        JSONArray methodParams = new JSONArray();
        JSONArray paramsArray = new JSONArray();
        JSONArray idsJsonArray = new JSONArray();

        for (int i = 0; i < attachmentIds.size(); i++) {
            idsJsonArray.put(attachmentIds.get(i));
        }

        /*filtering if message belongs to this channel*/
        JSONArray itemsJsonArray = new JSONArray();
        itemsJsonArray.put("id");
        itemsJsonArray.put("in");
        itemsJsonArray.put(idsJsonArray);
        paramsArray.put(itemsJsonArray);
        methodParams.put(paramsArray);

        /*needed fields to be returned from the model*/
        JSONArray showItems = new JSONArray();
        boolean limit = false;
        int offset = 0;

        searchRead(methodParams, showItems, url, limit, offset, "ir.attachment", callback, JSONObject.class);
    }


    @Override
    public void getAttachment(int id, ClientCallback callback) {
        String url = getRemoteUrl() + ODOO_JSON_RPC;

         /*search criteria filter*/
        JSONArray methodParams = new JSONArray();
        JSONArray paramsArray = new JSONArray();

        JSONArray itemsJsonArray = new JSONArray();
        itemsJsonArray.put("id");
        itemsJsonArray.put("=");
        itemsJsonArray.put(id);
        paramsArray.put(itemsJsonArray);
        methodParams.put(paramsArray);

        /*needed fields to be returned from the model*/
        JSONArray showItems = new JSONArray();
        boolean limit = false;
        int offset = 0;

        searchRead(methodParams, showItems, url, limit, offset, "ir.attachment", callback, JSONObject.class);
    }

    @Override
    public void getTrackingCodes(ArrayList<Integer> trackingCodeIds, ClientCallback callback) {
        String url = getRemoteUrl() + ODOO_JSON_RPC;
        JSONArray methodParams = new JSONArray();
        JSONArray paramsArray = new JSONArray();

        JSONArray itemsJsonArray = new JSONArray();
        itemsJsonArray.put("id");
        itemsJsonArray.put("in");

        JSONArray idsJsonArray = new JSONArray();

        for (int i = 0; i < trackingCodeIds.size(); i++) {
            idsJsonArray.put(Integer.parseInt(trackingCodeIds.get(i).toString()));
        }

        itemsJsonArray.put(idsJsonArray);
        paramsArray.put(itemsJsonArray);
        methodParams.put(paramsArray);

        /*needed fields to be returned from the model*/
        JSONArray showItems = new JSONArray();
        boolean limit = false;
        int offset = 0;

        searchRead(methodParams, showItems, url, limit, offset, "mail.tracking.value", callback, JSONObject.class);

    }

    @Override
    public void getUserMessages(ArrayList messagesIds, ClientCallback callback) {
        String url = getRemoteUrl() + ODOO_JSON_RPC;

        JSONArray methodParams = new JSONArray();
        JSONArray paramsArray = new JSONArray();

        JSONArray itemsJsonArray = new JSONArray();
        itemsJsonArray.put("id");
        itemsJsonArray.put("in");

        JSONArray idsJsonArray = new JSONArray();

        for (int i = 0; i < messagesIds.size(); i++) {
            idsJsonArray.put(Integer.parseInt(messagesIds.get(i).toString()));
        }

        itemsJsonArray.put(idsJsonArray);
//        itemsJsonArray.put(SharedPref.LoadInt(context, SharedPref.USER_ID));
        paramsArray.put(itemsJsonArray);

        methodParams.put(paramsArray);

        /*needed fields to be returned from the model*/
        JSONArray showItems = new JSONArray();
        boolean limit = false;
        int offset = 0;

        searchRead(methodParams, showItems, url, limit, offset, "mail.message", callback, JSONObject.class);

    }

    @Override
    public void getChannelMessages(int channelId, ClientCallback callback) {
        String url = getRemoteUrl() + ODOO_JSON_RPC;

         /*search criteria filter*/
        JSONArray methodParams = new JSONArray();
        JSONArray paramsArray = new JSONArray();

        JSONArray itemsJsonArray = new JSONArray();
        itemsJsonArray.put("channel_ids");
        itemsJsonArray.put("in");
        itemsJsonArray.put(channelId);
        paramsArray.put(itemsJsonArray);

        methodParams.put(paramsArray);

        /*needed fields to be returned from the model*/
        JSONArray showItems = new JSONArray();
        boolean limit = false;
        int offset = 0;

        searchRead(methodParams, showItems, url, limit, offset, "write_date asc", "mail.message", callback, JSONObject.class);
    }
}
