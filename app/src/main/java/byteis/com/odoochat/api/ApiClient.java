package byteis.com.odoochat.api;

import java.util.ArrayList;

import byteis.com.odooapilibrary.api.ClientCallback;
import byteis.com.odoochat.models.Attachment;

/**
 * Created by Dina on 13/11/2016.
 */

public interface ApiClient extends byteis.com.odooapilibrary.api.ApiClient {

    String CREATE_METHOD = "create";
    String ATTACHMENT_MODEL = "ir.attachment";

    void getUserSessions(int uid, ClientCallback callback);

    void getUsers(ClientCallback callback);

    void getChannels(ClientCallback callback, String channelType, String privateState, boolean isPublic);

    void getMessages(ArrayList messageIds, int channelId, ClientCallback callback);

    void getUserMessages(ArrayList messagesIds, ClientCallback callback);

    void getPartners(ArrayList<Integer> partnerIds, ClientCallback callback);

    void getUser(ClientCallback callback, int userId);

    void sendMessage(String messageBody, int selectedChannelId, ClientCallback callback);

    void getAttachment(ArrayList<Integer> attachmentIds, ClientCallback callback);

    void getTrackingCodes(ArrayList<Integer> trackingCodeIds, ClientCallback callback);

    void createAttachment(Attachment attachment, ClientCallback callback);

    void createMessage(ArrayList<Integer> attachmentIds, int channelId, ClientCallback callback);

    void getAttachment(int id, ClientCallback callback);

    void getChannelMessages(int channelId, ClientCallback callback);


    void getFilteredChannels(ClientCallback callback);
}
