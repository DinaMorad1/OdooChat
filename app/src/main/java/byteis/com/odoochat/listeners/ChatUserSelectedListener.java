package byteis.com.odoochat.listeners;

import byteis.com.odoochat.models.Channel;

/**
 * Created by Dina on 14/11/2016.
 */

public interface ChatUserSelectedListener {

    void onChannelSelected(Channel channel, int selectedPosition);

}
