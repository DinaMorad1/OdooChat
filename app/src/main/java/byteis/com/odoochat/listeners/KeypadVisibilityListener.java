package byteis.com.odoochat.listeners;

/**
 * Created by Dina on 17/11/2016.
 */

public interface KeypadVisibilityListener {

    void onKeyboardVisibilityChanged(boolean keyboardVisible);

}
